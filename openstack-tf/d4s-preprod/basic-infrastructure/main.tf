# Define required providers
terraform {
required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.53.0"
    }
  }
}

data "terraform_remote_state" "privnet_dns_router" {
  backend = "local"

  config = {
    path = "../project-setup/terraform.tfstate"
  }
}

module "ssh_settings" {
  source = "../../modules/ssh-key-ref"
}
# module "common_variables" {
#   source = "../../modules/common_variables"
# }
#module "d4science_infra_setup" {
#  source = "../../modules/d4science_infra_setup"
#}
