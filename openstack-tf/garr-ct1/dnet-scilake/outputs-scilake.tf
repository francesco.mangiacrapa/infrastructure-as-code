output "scilake_instance" {
  value = openstack_compute_instance_v2.scilake_intelcomp_server
  sensitive = true
}

output "scilake_floating_ip" {
  value = openstack_networking_floatingip_v2.scilake_catalogue_server_ip
}
