# Define required providers
terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source = "terraform-provider-openstack/openstack"
    }
  }
}

resource "openstack_networking_network_v2" "main-private-network" {
  name                  = var.main_private_network.name
  admin_state_up        = "true"
  external              = "false"
  description           = var.main_private_network.description
  mtu                   = var.mtu_size
  port_security_enabled = true
  shared                = false
  region                = var.os_region_data.name
  tenant_id             = var.os_project_data.id
}

resource "openstack_networking_subnet_v2" "main-private-subnet" {
  name            = var.main_private_subnet.name
  description     = var.main_private_subnet.description
  network_id      = openstack_networking_network_v2.main-private-network.id
  cidr            = var.main_private_subnet.cidr
  gateway_ip      = var.main_private_subnet.gateway_ip
  dns_nameservers = var.resolvers_ip
  ip_version      = 4
  enable_dhcp     = true
  tenant_id       = var.os_project_data.id
  allocation_pool {
    start = var.main_private_subnet.allocation_start
    end   = var.main_private_subnet.allocation_end
  }
}

resource "openstack_networking_router_v2" "external-router" {
  name                = var.external_router.name
  description         = var.external_router.description
  external_network_id = var.external_network.id
  tenant_id           = var.os_project_data.id
  enable_snat         = true
  vendor_options {
    set_router_gateway_after_create = true
  }
}

# Router interface configuration
resource "openstack_networking_router_interface_v2" "private-network-routing" {
  router_id = openstack_networking_router_v2.external-router.id
  # router_id = var.external_router.id
  subnet_id = openstack_networking_subnet_v2.main-private-subnet.id
}

