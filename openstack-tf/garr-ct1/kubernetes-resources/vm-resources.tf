#
# Server groups for both the controllers and the workers
#
resource "openstack_compute_servergroup_v2" "k8s_controllers" {
  name     = "k8s_controllers"
  policies = ["soft-anti-affinity"]
}
resource "openstack_compute_servergroup_v2" "k8s_workers" {
  name     = "k8s_workers"
  policies = ["soft-anti-affinity"]
}

#
# Allow traffic between the cluster nodes
#
resource "openstack_networking_secgroup_v2" "k8s_internal_traffic" {
  name                 = "k8s_internal_traffic"
  delete_default_rules = "true"
  description          = "Traffic between the Docker k8s nodes"
}
resource "openstack_networking_secgroup_rule_v2" "everything_udp" {
  security_group_id = openstack_networking_secgroup_v2.k8s_internal_traffic.id
  description       = "UDP traffic between k8s nodes"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "udp"
  remote_ip_prefix  = data.terraform_remote_state.privnet_dns_router.outputs.main_private_subnet.cidr
}
resource "openstack_networking_secgroup_rule_v2" "everything_tcp" {
  security_group_id = openstack_networking_secgroup_v2.k8s_internal_traffic.id
  description       = "TCP traffic between k8s nodes"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  remote_ip_prefix  = data.terraform_remote_state.privnet_dns_router.outputs.main_private_subnet.cidr
}
resource "openstack_networking_secgroup_rule_v2" "igmp_ingress_between_k8s_nodes" {
  security_group_id = openstack_networking_secgroup_v2.k8s_internal_traffic.id
  description       = "Ingress IGMP traffic between k8s nodes"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "igmp"
  remote_ip_prefix  = data.terraform_remote_state.privnet_dns_router.outputs.main_private_subnet.cidr
}

resource "openstack_networking_secgroup_rule_v2" "igmp_egress_between_k8s_nodes" {
  security_group_id = openstack_networking_secgroup_v2.k8s_internal_traffic.id
  description       = "Egress IGMP traffic between k8s nodes"
  direction         = "egress"
  ethertype         = "IPv4"
  protocol          = "igmp"
  remote_ip_prefix  = data.terraform_remote_state.privnet_dns_router.outputs.main_private_subnet.cidr
}

# Controllers VMs
#
resource "openstack_compute_instance_v2" "docker_k8s_controllers" {
  count                   = var.k8s_controllers_data.count
  name                    = format("%s-%02d", var.k8s_controllers_data.name, count.index + 1)
  availability_zone_hints = data.terraform_remote_state.privnet_dns_router.outputs.availability_zone.name
  flavor_name             = var.k8s_controllers_data.flavor
  key_pair                = module.ssh_settings.ssh_key_name
  security_groups         = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group.name, openstack_networking_secgroup_v2.k8s_internal_traffic.name]
  scheduler_hints {
    group = openstack_compute_servergroup_v2.k8s_controllers.id
  }
  block_device {
    uuid                  = module.common_variables.ubuntu_2204.uuid
    source_type           = "image"
    volume_size           = var.k8s_controllers_data.boot_disk_size
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = false
  }

  block_device {
    source_type           = "blank"
    volume_size           = var.k8s_controllers_data.docker_disk_size
    boot_index            = -1
    destination_type      = "volume"
    delete_on_termination = false
  }

  block_device {
    source_type           = "blank"
    volume_size           = var.k8s_controllers_data.etcd_disk_size
    boot_index            = -1
    destination_type      = "volume"
    delete_on_termination = false
  }

  network {
    name = data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name
  }

  user_data = file("${module.common_variables.ubuntu2204_data_file}")
  # Do not replace the instance when the ssh key changes
  lifecycle {
    ignore_changes = [
      # Ignore changes to tags, e.g. because a management agent
      # updates these based on some ruleset managed elsewhere.
      key_pair, user_data, network
    ]
  }
}

# k8s worker nodes
resource "openstack_compute_instance_v2" "docker_k8s_workers" {
  count                   = var.k8s_workers_data.count
  name                    = format("%s-%02d", var.k8s_workers_data.name, count.index + 1)
  availability_zone_hints = data.terraform_remote_state.privnet_dns_router.outputs.availability_zone.name
  flavor_name             = var.k8s_workers_data.flavor
  key_pair                = module.ssh_settings.ssh_key_name
  security_groups         = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group.name, openstack_networking_secgroup_v2.k8s_internal_traffic.name]
  scheduler_hints {
    group = openstack_compute_servergroup_v2.k8s_workers.id
  }
  block_device {
    uuid                  = module.common_variables.ubuntu_2204.uuid
    source_type           = "image"
    volume_size           = var.k8s_workers_data.boot_disk_size
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = false
  }

  block_device {
    source_type           = "blank"
    volume_size           = var.k8s_workers_data.docker_disk_size
    boot_index            = -1
    destination_type      = "volume"
    delete_on_termination = false
  }

  network {
    name = data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name
  }

  user_data = file("${module.common_variables.ubuntu2204_data_file}")
  # Do not replace the instance when the ssh key changes
  lifecycle {
    ignore_changes = [
      # Ignore changes to tags, e.g. because a management agent
      # updates these based on some ruleset managed elsewhere.
      key_pair, user_data, network
    ]
  }
}
