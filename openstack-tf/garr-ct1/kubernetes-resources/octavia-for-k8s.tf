#
# Octavia
#
#
# Security group to open the ICMP traffic
#
resource "openstack_networking_secgroup_v2" "octavia_extra_traffic" {
  name                 = "octavia_lb_extra_traffic"
  delete_default_rules = "true"
  description          = "Traffic to the load balancer in addition to the listeners"
}

resource "openstack_networking_secgroup_rule_v2" "icmp_ingress_to_the_load_balancer" {
  security_group_id = openstack_networking_secgroup_v2.octavia_extra_traffic.id
  description       = "Ingress ICMP traffic"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "icmp"
  remote_ip_prefix  = "0.0.0.0/0"
}

resource "openstack_networking_secgroup_rule_v2" "ssh_ingress_to_the_load_balancer" {
  security_group_id = openstack_networking_secgroup_v2.octavia_extra_traffic.id
  description       = "Ingress SSH traffic from the internal network"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = data.terraform_remote_state.privnet_dns_router.outputs.main_private_subnet.cidr
}

# Kubernetes load balancer. L4, backed by Octavia
resource "openstack_lb_loadbalancer_v2" "k8s_lb" {
  vip_subnet_id         = data.terraform_remote_state.privnet_dns_router.outputs.main_private_subnet.id
  name                  = var.octavia_kubernetes_data.k8s_lb_name
  description           = var.octavia_kubernetes_data.k8s_lb_description
  # flavor_id             = var.octavia_kubernetes_data.octavia_flavor_id
  loadbalancer_provider = "amphora"
  security_group_ids = [openstack_networking_secgroup_v2.octavia_extra_traffic.id]
}

# Allocate a floating IP
resource "openstack_networking_floatingip_v2" "k8s_lb_ip" {
  pool = data.terraform_remote_state.privnet_dns_router.outputs.external_network.name
  # The DNS association does not work because of a bug in the OpenStack API
  # dns_name = "main-lb"
  # dns_domain = var.dns_zone.zone_name
  description = var.octavia_kubernetes_data.k8s_lb_description
}

resource "openstack_networking_floatingip_associate_v2" "k8s_lb" {
  floating_ip = openstack_networking_floatingip_v2.k8s_lb_ip.address
  port_id     = openstack_lb_loadbalancer_v2.k8s_lb.vip_port_id
}

# HAPROXY HTTP
resource "openstack_lb_listener_v2" "k8s_http_listener" {
  loadbalancer_id = openstack_lb_loadbalancer_v2.k8s_lb.id
  protocol        = "TCP"
  protocol_port   = 80
  description     = "HTTP listener of the k8s ingress"
  name            = "k8s_http_listener"
  admin_state_up  = true
}

resource "openstack_lb_pool_v2" "k8s_http_pool" {
  listener_id = openstack_lb_listener_v2.k8s_http_listener.id
  protocol    = "PROXYV2"
  lb_method   = "LEAST_CONNECTIONS"
  name        = "k8s-lb-http"
  description = "Pool for the HTTP listener of the k8s service"
  persistence {
    type = "SOURCE_IP"
  }
  admin_state_up = true
}

resource "openstack_lb_members_v2" "k8s_http_pool_members" {
  count   = var.k8s_controllers_data.count
  pool_id = openstack_lb_pool_v2.k8s_http_pool.id
  member {
    name          = format("k8s controller %02d", count.index + 1)
    address       = openstack_compute_instance_v2.docker_k8s_controllers[count.index].network.0.fixed_ip_v4
    protocol_port = 80
  }
}

# HAPROXY HTTPS
resource "openstack_lb_listener_v2" "k8s_https_listener" {
  loadbalancer_id = openstack_lb_loadbalancer_v2.k8s_lb.id
  protocol        = "TCP"
  protocol_port   = 443
  description     = "HTTPS listener of the main k8s service"
  name            = "k8s_https_listener"
  admin_state_up  = true
}

resource "openstack_lb_pool_v2" "k8s_https_pool" {
  listener_id = openstack_lb_listener_v2.k8s_https_listener.id
  protocol    = "PROXYV2"
  lb_method   = "LEAST_CONNECTIONS"
  name        = "k8s-lb-https"
  description = "Pool for the HTTPS listener of the k8s service"
  persistence {
    type = "SOURCE_IP"
  }
  admin_state_up = true
}

resource "openstack_lb_members_v2" "k8s_https_pool_members" {
  count   = var.k8s_controllers_data.count
  pool_id = openstack_lb_pool_v2.k8s_https_pool.id
  member {
    name          = format("k8s controller %02d", count.index + 1)
    address       = openstack_compute_instance_v2.docker_k8s_controllers[count.index].network.0.fixed_ip_v4
    protocol_port = 443
  }
}

output "k8s_loadbalancer_ip" {
  description = "Kubernetes Load balancer IP address"
  value       = openstack_lb_loadbalancer_v2.k8s_lb.vip_address
}
