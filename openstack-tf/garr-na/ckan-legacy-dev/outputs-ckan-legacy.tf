output "ckan_dev_legacy_instance" {
  value = openstack_compute_instance_v2.ckan_dev_legacy_server
  sensitive = true
}

output "ckan_dev_legacy_floating_ip" {
  value = openstack_networking_floatingip_v2.ckan_dev_legacy_floating_ip
}
