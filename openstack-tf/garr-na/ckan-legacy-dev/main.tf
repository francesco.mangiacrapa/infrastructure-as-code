# Define required providers
terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source = "terraform-provider-openstack/openstack"
      version = ">= 1.54.0"
    }
  }
}

data "terraform_remote_state" "privnet_dns_router" {
  backend = "local"

  config = {
    path = "../project-setup/terraform.tfstate"
  }
}

# SSH settings
module "ssh_settings" {
  source = "../../modules/ssh-key-ref"
}

# Global variables (constants, really)
module "common_variables" {
  source = "../../modules/garr_common_variables"
}

# SCILake server. All in one, everything docker
resource "openstack_blockstorage_volume_v3" "ckan_legacy_pg_vol" {
  name = "CKAN legacy postgresql data"
  size = 20
}

resource "openstack_blockstorage_volume_v3" "ckan_legacy_solr_vol" {
  name = "CKAN legacy SOLR data"
  size = 10
}

resource "openstack_networking_port_v2" "ckan_legacy_ip_in_main_net" {
  name           = "ckan_legacy_main"
  admin_state_up = "true"
  network_id     = data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.id
  security_group_ids = [
    data.terraform_remote_state.privnet_dns_router.outputs.default_security_group.id,
  ]
}

resource "openstack_compute_instance_v2" "ckan_dev_legacy_server" {
  name                    = "CKAN dev legacy server"
  availability_zone_hints = "nova"
  flavor_name             = "m2.medium"
  key_pair                = module.ssh_settings.ssh_key_name
  block_device {
    uuid                  = module.common_variables.ubuntu_2404.uuid
    source_type           = "image"
    volume_size           = 10
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = true
  }

  network {
    port = openstack_networking_port_v2.ckan_legacy_ip_in_main_net.id
  }

  user_data = file("${module.common_variables.ubuntu2404_data_file}")
  # Do not replace the instance when the ssh key changes
  lifecycle {
    ignore_changes = [
      # Ignore changes to tags, e.g. because a management agent
      # updates these based on some ruleset managed elsewhere.
      key_pair, user_data, network
    ]
  }

}

resource "openstack_compute_volume_attach_v2" "postgresql_data_attach_vol" {
  instance_id = openstack_compute_instance_v2.ckan_dev_legacy_server.id
  volume_id   = openstack_blockstorage_volume_v3.ckan_legacy_pg_vol.id
  device      = "/dev/vdb"
}

resource "openstack_compute_volume_attach_v2" "solr_data_attach_vol" {
  instance_id = openstack_compute_instance_v2.ckan_dev_legacy_server.id
  volume_id   = openstack_blockstorage_volume_v3.ckan_legacy_solr_vol.id
  device      = "/dev/vdc"
}

#
resource "openstack_networking_floatingip_v2" "ckan_dev_legacy_floating_ip" {
  pool        = data.terraform_remote_state.privnet_dns_router.outputs.external_network.name
  description = "CKAN dev legacy"
}

resource "openstack_networking_floatingip_associate_v2" "ckan_dev_legacy_server" {
  floating_ip = openstack_networking_floatingip_v2.ckan_dev_legacy_floating_ip.address
  port_id     = openstack_networking_port_v2.ckan_legacy_ip_in_main_net.id
}

