output "os_project_data" {
  value = var.os_project_data
}

output "os_region_data" {
  value = var.os_region_data

}
output "default_security_group" {
  value = var.default_security_group
}

output "availability_zone" {
  value = var.availability_zone
}

output "main_private_network" {
  value = var.main_private_network
}

output "mtu_size" {
  value = var.mtu_size
}

output "main_private_subnet" {
  value = var.main_private_subnet
}

output "resolvers_ip" {
  value = var.resolvers_ip
}

output "external_router" {
  value = var.external_router
}

output "external_network" {
  value = var.external_network
}

output "jump_proxy_ssh_shell" {
  value = var.jump_proxy_ssh_shell
}

output "prometheus_host" {
  value = var.prometheus_host
}
