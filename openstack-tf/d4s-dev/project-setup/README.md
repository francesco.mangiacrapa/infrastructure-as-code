# How to obtain the "generated" data

terraformer can obtain the resources of a region. On OpenStack it is limited to the compute, block storage and networks resources.

The command is

```shell-session
terraformer import openstack --resources="*" --regions isti_area_pi_1
```

and it requires the load of the shell application credentials, it does not understand the `clouds.yaml` ones.
