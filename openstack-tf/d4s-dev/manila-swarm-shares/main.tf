# Define required providers
terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      #version = "~> 1.53.0"
    }
  }
}

data "terraform_remote_state" "privnet_dns_router" {
  backend = "local"

  config = {
    path = "../project-setup/terraform.tfstate"
  }
}

# data "terraform_remote_state" "main_infrastructure" {
#   backend = "local"

#   config = {
#     path = "../basic-infrastructure/terraform.tfstate"
#   }
# }

# SSH settings
module "ssh_settings" {
  source = "../../modules/ssh-key-ref"
}
#
# Uses common_variables as module
#
module "common_variables" {
  source = "../../modules/common_variables"
}

variable "swarm_manila_interfaces_ip" {
  type = map(string)
  default = {
    "mgr_1" = "172.17.2.74"
    "mgr_2" = "172.17.3.218"
    "mgr_3" = "172.17.2.230"
    "worker_1" = "172.17.0.166"
    "worker_2" = "172.17.2.171"
    "worker_3" = "172.17.0.146"
    "worker_4" = "172.17.1.195"
    "worker_5" = "172.17.2.187"
  }
}
