# Define required providers
terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.53.0"
    }
  }
}

data "terraform_remote_state" "privnet_dns_router" {
  backend = "local"

  config = {
    path = "../project-setup/terraform.tfstate"
  }
}

# SSH settings
module "ssh_settings" {
  source = "../../modules/ssh-key-ref"
}
#
# Uses common_variables as module
#
module "common_variables" {
  source = "../../modules/common_variables"
}

# Create a NFS share
resource "openstack_sharedfilesystem_share_v2" "redmine_dev" {
  name        = "redmine_dev_nfs_share"
  description = "NFS share for the dev redmine"
  share_proto = "NFS"
  size        = 5
}

resource "openstack_networking_port_v2" "redmine_nfs_port" {
  name           = "redmine_nfs_port"
  network_id     = data.terraform_remote_state.privnet_dns_router.outputs.storage_nfs_network_id
  admin_state_up = "true"
  fixed_ip {
    subnet_id = data.terraform_remote_state.privnet_dns_router.outputs.storage_nfs_subnet_id
  }
}

resource "openstack_networking_port_secgroup_associate_v2" "redmine_nfs_port_secgroup" {
  port_id = openstack_networking_port_v2.redmine_nfs_port.id
  security_group_ids = [
    data.terraform_remote_state.privnet_dns_router.outputs.nfs_share_no_ingress_secgroup_id
  ]
}

resource "openstack_compute_instance_v2" "redmine" {
  name                    = "redmine-dev"
  availability_zone_hints = module.common_variables.availability_zones_names.availability_zone_no_gpu
  flavor_name             = module.common_variables.flavor_list.m1_medium
  key_pair                = module.ssh_settings.ssh_key_name
  security_groups         = [data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.default, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.nfs_share_no_ingress]
  block_device {
    uuid                  = data.terraform_remote_state.privnet_dns_router.outputs.ubuntu_2204.uuid
    source_type           = "image"
    volume_size           = 20
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = true
  }
  network {
    name = data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name
  }
  network {
    name = module.common_variables.shared_postgresql_server_data.network_name
  }
  user_data = file("${data.terraform_remote_state.privnet_dns_router.outputs.ubuntu2204_data_file}")
  # Do not replace the instance when the ssh key changes
  lifecycle {
    ignore_changes = [
      # Ignore changes to tags, e.g. because a management agent
      # updates these based on some ruleset managed elsewhere.
      key_pair, user_data, network
    ]
  }
}

resource "openstack_compute_interface_attach_v2" "nfs_port_to_redmine" {
  instance_id = openstack_compute_instance_v2.redmine.id
  port_id     = openstack_networking_port_v2.redmine_nfs_port.id
}

# Allow access to the NFS share
resource "openstack_sharedfilesystem_share_access_v2" "redmine_dev_nfs_share_access" {
  share_id     = openstack_sharedfilesystem_share_v2.redmine_dev.id
  access_type  = "ip"
  access_to    = openstack_compute_interface_attach_v2.nfs_port_to_redmine.fixed_ip
  access_level = "rw"
}
