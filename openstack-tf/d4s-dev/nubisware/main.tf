# Define required providers
terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.53.0"
    }
  }
}

data "terraform_remote_state" "privnet_dns_router" {
  backend = "local"

  config = {
    path = "../project-setup/terraform.tfstate"
  }
}


#
# Uses common_variables as module
#
module "common_variables" {
  source = "../../modules/common_variables"
}

#
# Creates the server group "nubisware-cluster"
# Even in dev because this service is crucial the server group is
# created with anti-affinity policy
#
resource "openstack_compute_servergroup_v2" "nubisware-cluster_server_group" {
  name     = "nubisware-cluster"
  policies = [module.common_variables.policy_list.anti_affinity]
}

module "instance_with_data_volume" {
  source = "../../modules/instance_with_data_volume"

  instances_with_data_volume_map = {
    nubisware1 = {
      name              = "nubis1",
      description       = "Nubisware cluster nubis1 instance",
      flavor            = module.common_variables.flavor_list.m1_large,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.shared_postgresql],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.default, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      server_groups_ids = [],
      image_ref         = data.terraform_remote_state.privnet_dns_router.outputs.ubuntu_2204,
      volume = {
        name   = "nubis1_data_volume",
        size   = "100",
        device = "/dev/vdb",
      }
    },
    nubis2 = {
      name              = "nubis2",
      description       = "Nubisware cluster nubis2 instance",
      flavor            = module.common_variables.flavor_list.m1_medium,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.shared_postgresql],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.default, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      server_groups_ids = [],
      image_ref         = data.terraform_remote_state.privnet_dns_router.outputs.ubuntu_2204,
      volume = {
        name   = "nubis2_data_volume",
        size   = "50",
        device = "/dev/vdb",
      }
    },
    nubis3 = {
      name              = "nubis3",
      description       = "Nubisware cluster nubis3 instance",
      flavor            = module.common_variables.flavor_list.m1_medium,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.shared_postgresql],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.default, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      server_groups_ids = [],
      image_ref         = data.terraform_remote_state.privnet_dns_router.outputs.ubuntu_2204,
      volume = {
        name   = "nubis3_data_volume",
        size   = "50",
        device = "/dev/vdb",
      }
    }

  }


}
