#!/bin/bash

/usr/sbin/adduser --system --home /srv/ansible --shell /bin/bash --gecos "Used for the Ansible provisioning tasks" --group ansible

# SSH keys of users authorized to execute ansible playbooks.
# The ones in the example belong to Andrea Dell'Amico and Tommaso Piccioli.
# Feel free to add yours if you are entitled to run the ansible provisioning on that server

mkdir /srv/ansible/.ssh
echo "ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAzcHuDU7PgJwz34AsVG0E2+ZRx17ZKW1uDEGABNk3Z60/c9LTwWKPj6kcIRy6RzFJI5X+IgPJnYouXVmJsIWjVL8IRk8fP1ffJC6Fyf6H7+fCxu/Wwed5OoOCvKeZ0bEmJ1tlXFM6+EnxKqLCvz3fsNy8e4WKMnpS1hT8K6YB7PMjt60S3wOaxds1Lv4NmmgnfGM5uZFYrZCx1/GJCzNSh7AEEEUIVQ1B8xmXbet7whNiwDmiOnXSlt38dkIYT8kNMuRCj/r9wPr7FmoUCOFzUVXTcnuYagKyURrZ8QDyHbK6XQLYXgvCz/lWoErGFbDqpmBHHyvKSeLPxYfJpWJ70w== tom@tom" > /srv/ansible/.ssh/authorized_keys
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDZ9n6B+J5S7NPnwjejPC2WrvcRzC07WPnAoQ7ZHZ0Mv9JakyWItswzI3Drz/zI0mCamyuye+9dWz9v/ZRwUfBobVyXuptRaZIwxlMC/KsTZofpp3RHOBTteZ4/VM0VhEeiOHu+GuzNE0fRB2gsusWeMMae2cq4TjVAOMcQmJX496L703Smc14gFrP8y/P9jbC5HquuVnPR29PsW4mHidPmjdKkO7QmDfFAj44pEUGeInYOJe708C03NCpsjHw8AVdAJ6Pf16EOdDH+z8D6CByVO3s8UT0HJ85BRoIy6254/hmYLzyd/eRnCXHS/dke+ivrlA3XxG4+DmqjuJR/Jpfx adellam@semovente" >> /srv/ansible/.ssh/authorized_keys
echo "ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAQEAjmoNSXtyx1TKOr8eJDfO1mRdX0JxQmf5N9OnMrqtF3DpJCWslws5NIDQBEsERPhdHlljUM9Y6lXpt5CWiBHH+DiSo1tE4FQ74l41tkwVqW9tGrFinWEaGRfb84KYJc0vmW5lPdGGKY+KKlhKDF1TFcTGQfkh6ZD4BXU1Uc11nR11XD3OAAcltNfuw9biN1kUQjMCRztytJEOW1nfLtEufaZMGoE8+RMS8PVWlPcjTe/ACkEixvimwGEiyZEi2uiWIqocja/wyKCJikgrWB6Zg24bouM564V4n+0JUomNgVSjFT8TXv5hecNt8k3HRMGMyLaeaGiafbsNgicG3s3beQ== g.panichi@isti.cnr.it" >> /srv/ansible/.ssh/authorized_keys
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDa0NzwaCcauxAFlsupU2xG2eff9nzep9bnb8pISbX2lk+K4yoJvJOAz9W9klJtpPX/IUJx18YR4jjDNcdiYWNh4Y+5jKT2EhSPNkj7Vw2MhA/ZeOrfHx7JNtL8gdxa8XxYB0ZoZqutRppmaRwWmGGwdVh0wyUzWR/v0OT01IuQGYVneLKIjUtx+BcWGsosWISaOQzVbv9iTFbSwgjbkKFHzHasxwKsrK4t1wvbzuxwhVC+5/VKghBJWN219m/PO+itww/fSes0KpI5X/7q8jrYzUgYwrKwt290U41Fx8syDQ6101YnRzMXZRyZwuVNh2S7WosGWebg5nPS4IjKho/F francesco-mangiacrapa@ubuntu-francesco-i24" >> /srv/ansible/.ssh/authorized_keys
/bin/chown -R ansible:ansible /srv/ansible
/bin/chmod 700 /srv/ansible/.ssh
mkdir -p /etc/sudoers.d
echo "ansible ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/ansible-user
/bin/chmod 600 /etc/sudoers.d/ansible-user

