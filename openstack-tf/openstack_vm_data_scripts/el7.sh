#!/bin/bash

yum -y install python policycoreutils-python

/sbin/useradd --system --home-dir /srv/ansible -m --shell /bin/bash -c "Used for the Ansible provisioning tasks" ansible

# SSH keys of users authorized to execute ansible playbooks.
# The ones in the example belong to Andrea Dell'Amico and Tommaso Piccioli.
# Feel free to add yours if you are entitled to run the ansible provisioning on that server

mkdir /srv/ansible/.ssh
echo "ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAzcHuDU7PgJwz34AsVG0E2+ZRx17ZKW1uDEGABNk3Z60/c9LTwWKPj6kcIRy6RzFJI5X+IgPJnYouXVmJsIWjVL8IRk8fP1ffJC6Fyf6H7+fCxu/Wwed5OoOCvKeZ0bEmJ1tlXFM6+EnxKqLCvz3fsNy8e4WKMnpS1hT8K6YB7PMjt60S3wOaxds1Lv4NmmgnfGM5uZFYrZCx1/GJCzNSh7AEEEUIVQ1B8xmXbet7whNiwDmiOnXSlt38dkIYT8kNMuRCj/r9wPr7FmoUCOFzUVXTcnuYagKyURrZ8QDyHbK6XQLYXgvCz/lWoErGFbDqpmBHHyvKSeLPxYfJpWJ70w== tom@tom" > /srv/ansible/.ssh/authorized_keys
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDZ9n6B+J5S7NPnwjejPC2WrvcRzC07WPnAoQ7ZHZ0Mv9JakyWItswzI3Drz/zI0mCamyuye+9dWz9v/ZRwUfBobVyXuptRaZIwxlMC/KsTZofpp3RHOBTteZ4/VM0VhEeiOHu+GuzNE0fRB2gsusWeMMae2cq4TjVAOMcQmJX496L703Smc14gFrP8y/P9jbC5HquuVnPR29PsW4mHidPmjdKkO7QmDfFAj44pEUGeInYOJe708C03NCpsjHw8AVdAJ6Pf16EOdDH+z8D6CByVO3s8UT0HJ85BRoIy6254/hmYLzyd/eRnCXHS/dke+ivrlA3XxG4+DmqjuJR/Jpfx adellam@semovente" >> /srv/ansible/.ssh/authorized_keys
echo "ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAvkwppFE+K5MjKqtkGJN63wkcwaqZG4HkgPqMSWrXmCfDPJ3FxjDHV9aQRJYVKZObc9+SsFc9IYXwB2A8FI0XwPkCH2hfFKDVNO4TktO/SrM+4tXbEfEDWX/PduBQLootYaMEVj++p2+s/mxVnxTAMzsR4txC9tkWR4JO4VJ2cpZfM8po4p1wA4YteW6Oiv0PqUEsLtPtBHGuCgovo8WS+qxcxpeBBnewEssgis2dzDSqx5HUmaOETAxxEHflapHWQLum0JjvXsG5jlf9jL44XJPkcHXAYk3gnhtyM0moJpUya+GX7+ttfWWvwxs0tYNDXNMRn91r1hMLWmas4D+T/Q== rcirillo@rcirillo-cnr" >> /srv/ansible/.ssh/authorized_keys
/bin/chown -R ansible:ansible /srv/ansible
/bin/chmod 700 /srv/ansible/.ssh
mkdir -p /etc/sudoers.d
echo "ansible ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/ansible-user
/bin/chmod 600 /etc/sudoers.d/ansible-user

semanage fcontext -a -e /home /srv/ansible ; restorecon -vR /srv/ansible

