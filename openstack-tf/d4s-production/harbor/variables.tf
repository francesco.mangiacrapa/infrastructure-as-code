#Harbor  variables

variable "harbor_data" {
  type = map(string)
  default = {
    name              = "harbor"
    flavor            = "m1.xxl"
    vol_data_name     = "harbor-data"
    vol_data_size     = "20"
    vol_data_device   = "/dev/vdc"
    vol_docker_name   = "harbor-docker"
    vol_docker_size   = "2000"
    vol_docker_device = "/dev/vdb"
    object_store_name = "harbor-registry"
  }
}
