# Define required providers
terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = ">= 1.54.0"
    }
  }
}

data "terraform_remote_state" "privnet_dns_router" {
  backend = "local"

  config = {
    path = "../project-setup/terraform.tfstate"
  }
}

data "terraform_remote_state" "infrastructure_setup" {
  backend = "local"

  config = {
    path = "../basic-infrastructure/terraform.tfstate"
  }
}

#
# Uses common_variables as module
#
module "common_variables" {
  source = "../../modules/common_variables"
}

# Module used
module "ssh_settings" {
  source = "../../modules/ssh-key-ref"
}

# Creating object bucket to store avatars
resource "openstack_objectstorage_container_v1" "harbor_registry" {
  name       = var.harbor_data.object_store_name
  versioning = false
}

resource "openstack_networking_secgroup_v2" "harbor_access_list" {
  name                 = "harbor_access_list"
  delete_default_rules = "true"
  description          = "Allowed connections to the harbor server"
}

resource "openstack_networking_secgroup_rule_v2" "https_access_to_the_harbor_server" {
  security_group_id = openstack_networking_secgroup_v2.harbor_access_list.id
  description       = "HTTPS access to the Harbor Server"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 443
  port_range_max    = 443
  remote_ip_prefix  = "0.0.0.0/0"
}

resource "openstack_networking_secgroup_rule_v2" "http_access_to_the_harbor_server" {
  security_group_id = openstack_networking_secgroup_v2.harbor_access_list.id
  description       = "HTTP access to the Harbor Server"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 80
  port_range_max    = 80
  remote_ip_prefix  = "0.0.0.0/0"
}

# Block device
resource "openstack_blockstorage_volume_v3" "harbor_data_vol" {
  name = var.harbor_data.vol_data_name
  size = var.harbor_data.vol_data_size
}

resource "openstack_blockstorage_volume_v3" "harbor_docker_vol" {
  name = var.harbor_data.vol_docker_name
  size = var.harbor_data.vol_docker_size
}

#
# Port in the postgresql network
resource "openstack_networking_port_v2" "harbor_port_on_the_postgresql_net" {
  name           = "harbor_port_on_main_net"
  network_id     = data.terraform_remote_state.infrastructure_setup.outputs.shared_postgresql_network_data.id
  admin_state_up = "true"
  fixed_ip {
    subnet_id = data.terraform_remote_state.infrastructure_setup.outputs.shared_postgresql_subnet_data.id
  }
  security_group_ids = [data.terraform_remote_state.infrastructure_setup.outputs.vm_access_to_the_shared_postgresql_server.id]
}

# Instance
resource "openstack_compute_instance_v2" "harbor_server" {
  name = var.harbor_data.name
  # availability_zone_hints = module.common_variables.availability_zone_no_gpu_name
  flavor_name     = var.harbor_data.flavor
  key_pair        = module.ssh_settings.ssh_key_name
  security_groups = [data.terraform_remote_state.infrastructure_setup.outputs.default_security_group.name, openstack_networking_secgroup_v2.harbor_access_list.name]
  block_device {
    uuid                  = module.common_variables.ubuntu_2204.uuid
    source_type           = "image"
    volume_size           = 10
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = false
  }

  network {
      name = data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name
  }

  user_data = file("${module.common_variables.ubuntu2204_data_file}")
  # Do not replace the instance when the ssh key changes
  lifecycle {
    ignore_changes = [
      # Ignore changes to tags, e.g. because a management agent
      # updates these based on some ruleset managed elsewhere.
      key_pair, user_data, network
    ]
  }
}

resource "openstack_compute_volume_attach_v2" "harbor_data_attach_vol" {
  instance_id = openstack_compute_instance_v2.harbor_server.id
  volume_id   = openstack_blockstorage_volume_v3.harbor_data_vol.id
  device      = var.harbor_data.vol_data_device
  depends_on  = [openstack_compute_instance_v2.harbor_server]
}

resource "openstack_compute_volume_attach_v2" "harbor_docker_attach_vol" {
  instance_id = openstack_compute_instance_v2.harbor_server.id
  volume_id   = openstack_blockstorage_volume_v3.harbor_docker_vol.id
  device      = var.harbor_data.vol_docker_device
  depends_on  = [openstack_compute_instance_v2.harbor_server]
}

resource "openstack_networking_floatingip_v2" "harbor_ip" {
  pool = data.terraform_remote_state.privnet_dns_router.outputs.floating_ip_pools.main_public_ip_pool
  # The DNS association does not work because of a bug in the OpenStack API
  description = "Harbor ip"
}

resource "openstack_compute_floatingip_associate_v2" "harbor_floatingip_associate" {
  instance_id = openstack_compute_instance_v2.harbor_server.id
  floating_ip = openstack_networking_floatingip_v2.harbor_ip.address
  fixed_ip = openstack_compute_instance_v2.harbor_server.network[0].fixed_ip_v4
}

resource "openstack_compute_interface_attach_v2" "postgresql_network_to_harbor" {
  instance_id = openstack_compute_instance_v2.harbor_server.id
  port_id     = openstack_networking_port_v2.harbor_port_on_the_postgresql_net.id
}

locals {
  harbor_recordset_name = "harbor.${data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name}"
}

resource "openstack_dns_recordset_v2" "harbor_recordset" {
  zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
  name        = local.harbor_recordset_name
  description = "Public IP address of the Harbor Server"
  ttl         = 8600
  type        = "A"
  records     = [openstack_networking_floatingip_v2.harbor_ip.address]
}

output "harbor_public_ip_address" {
  value = openstack_networking_floatingip_v2.harbor_ip.address
}

output "harbor_hostname" {
  value = openstack_dns_recordset_v2.harbor_recordset.name
}
