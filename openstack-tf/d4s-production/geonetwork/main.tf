# Define required providers
terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = ">= 1.54.0"
    }
  }
}

data "terraform_remote_state" "privnet_dns_router" {
  backend = "local"

  config = {
    path = "../project-setup/terraform.tfstate"
  }
}
#
# Uses common_variables as module
#
module "common_variables" {
  source = "../../modules/common_variables"
}

module "instance_with_data_volume" {
  source = "../../modules/instance_with_data_volume"

  instances_with_data_volume_map = {
    geonetwork_grsf = {
      name              = "geonetwork-grsf",
      description       = "This instance serves geonetwork service",
      flavor            = module.common_variables.flavor_list.m2_small,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.shared_postgresql],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      server_groups_ids = [],
      image_ref         = module.common_variables.ubuntu_1804
      volume = {
        name   = "geonetwork_grsf_data_volume",
        size   = "20",
        device = "/dev/vdb"
      }
    },
    geonetwork_sdilab = {
      name              = "geonetwork-sdilab",
      description       = "This instance serves geonetwork service",
      flavor            = module.common_variables.flavor_list.m2_small,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.shared_postgresql],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      server_groups_ids = [],
      image_ref         = module.common_variables.ubuntu_1804
      volume = {
        name   = "geonetwork_sdilab_data_volume",
        size   = "20",
        device = "/dev/vdb"
      }
    },
    geonetwork_spatialdata = {
      name              = "geonetwork-spatialdata",
      description       = "This instance serves geonetwork service",
      flavor            = module.common_variables.flavor_list.m2_small,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.shared_postgresql],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      image_ref         = module.common_variables.ubuntu_1804
      server_groups_ids = [],
      volume = {
        name   = "geonetwork_spatialdata_data_volume",
        size   = "20",
        device = "/dev/vdb"
      }
    },
    geonetwork_wecafcfirms = {
      name              = "geonetwork-wecafcfirms",
      description       = "This instance serves geonetwork service",
      flavor            = module.common_variables.flavor_list.m2_small,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.shared_postgresql],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      image_ref         = module.common_variables.ubuntu_1804
      server_groups_ids = [],
      volume = {
        name   = "geonetwork_wecafcfirms_data_volume",
        size   = "20",
        device = "/dev/vdb"
      }
    },
    geonetwork_bluecloud = {
      name              = "geonetwork-bluecloud",
      description       = "This instance serves geonetwork service",
      flavor            = module.common_variables.flavor_list.m2_small,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.shared_postgresql],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      image_ref         = module.common_variables.ubuntu_1804
      server_groups_ids = [],
      volume = {
        name   = "geonetwork_bluecloud_data_volume",
        size   = "30",
        device = "/dev/vdb"
      }
    },
    geonetwork_globalfisheriesatlas = {
      name              = "geonetwork-globalfisheriesatlas",
      description       = "This instance serves geonetwork service",
      flavor            = module.common_variables.flavor_list.m2_small,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.shared_postgresql],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      image_ref         = module.common_variables.ubuntu_1804
      server_groups_ids = [],
      volume = {
        name   = "geonetwork_globalfisheriesatlas_data_volume",
        size   = "20",
        device = "/dev/vdb"
      }
    },
    geonetwork_itineris = {
      name              = "geonetwork-itineris",
      description       = "This instance serves geonetwork service",
      flavor            = module.common_variables.flavor_list.m2_small,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.shared_postgresql],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      image_ref         = module.common_variables.ubuntu_1804
      server_groups_ids = [],
      volume = {
        name   = "geonetwork_itineris_data_volume",
        size   = "20",
        device = "/dev/vdb"
      }
    },
    geonetwork_gaiablulab = {
      name              = "geonetwork-gaiablulab",
      description       = "This instance serves geonetwork service",
      flavor            = module.common_variables.flavor_list.m2_small,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.shared_postgresql],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      image_ref         = module.common_variables.ubuntu_1804
      server_groups_ids = [],
      volume = {
        name   = "geonetwork_gaiablulab_data_volume",
        size   = "20",
        device = "/dev/vdb"
      }
    },
    geonetwork_aquacultureatlas = {
      name              = "geonetwork-aquacultureatlas",
      description       = "This instance serves geonetwork service",
      flavor            = module.common_variables.flavor_list.m2_small,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.shared_postgresql],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      server_groups_ids = [],
      image_ref         = module.common_variables.ubuntu_1804
      volume = {
        name   = "geonetwork_aquacultureatlas_data_volume",
        size   = "20",
        device = "/dev/vdb"
      }
    }
    # geonetwork_recofi_rdb_pilot = {
    #   name              = "geonetwork-recofi-rdb-pilot",
    #   description       = "This instance serves geonetwork service",
    #   flavor            = module.common_variables.flavor_list.m2_small,
    #   networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.shared_postgresql],
    #   security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
    #   image_ref         = module.common_variables.ubuntu_1804
    #   server_groups_ids = [],
    #   volume = {
    #     name   = "geonetwork_recofi_rdb_pilot_data_volume",
    #     size   = "20",
    #     device = "/dev/vdb"
    #   }
    # }
  }
}

locals {
  cname_target = "main-lb.${data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name}"
}

#
# Add DNS record/s
#
module "dns_records_create" {
  source = "../../modules/dns_resources"

  dns_resources_map = {
    geonetwork-grsf = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["geonetwork-grsf", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "Geonetwork geonetwork-grsf"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    geonetwork-sdilab = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["geonetwork-sdilab", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "Geonetwork geonetwork-sdilab"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    geonetwork-spatialdata = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["geonetwork-spatialdata", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "Geonetwork geonetwork-spatialdata"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    geonetwork-wecafcfirms = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["geonetwork-wecafcfirms", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "Geonetwork geonetwork-wecafcfirms"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    geonetwork-bluecloud = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["geonetwork-bluecloud", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "Geonetwork geonetwork-bluecloud"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    geonetwork-globalfisheriesatlas = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["geonetwork-globalfisheriesatlas", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "Geonetwork geonetwork-globalfisheriesatlas"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    geonetwork-itineris = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["geonetwork-itineris", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "Geonetwork geonetwork-itineris"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    geonetwork-gaiablulab = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["geonetwork-gaiablulab", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "Geonetwork geonetwork-gaiablulab"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    geonetwork-aquacultureatlas = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["geonetwork-aquacultureatlas", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "Geonetwork geonetwork-aquacultureatlas"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    }
    # geonetwork-recofi-rdb-pilot = {
    #   zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
    #   name        = join(".", ["geonetwork-recofi-rdb-pilot", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
    #   description = "Geonetwork geonetwork-recofi-rdb-pilot"
    #   ttl         = 8600
    #   type        = "CNAME"
    #   records     = [local.cname_target]
    # }
  }
}
