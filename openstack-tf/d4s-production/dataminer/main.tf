# Define required providers
terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.53.0"
    }
  }
}

data "terraform_remote_state" "privnet_dns_router" {
  backend = "local"

  config = {
    path = "../project-setup/terraform.tfstate"
  }
}
#
# Uses common_variables as module
#
module "common_variables" {
  source = "../../modules/common_variables"
}

#Module used
module "ssh_settings" {
  source = "../../modules/ssh-key-ref"
}


resource "openstack_networking_secgroup_v2" "dataminer_publish" {
  name        = "dataminer_publish"
  description = "Access to dataminer-ghost is allowed only to dm-pool-manager"
  delete_default_rules = "true"
}


resource "openstack_compute_instance_v2" "dm_pool_manager_proto" {
  name   = "dm-pool-manager-proto"
  availability_zone_hints = module.common_variables.availability_zone_no_gpu_name
  flavor_name             = module.common_variables.flavor_list.m1_large
  key_pair                = module.ssh_settings.ssh_key_name
  security_groups         = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers]
  block_device {
    uuid                  = module.common_variables.ubuntu_1804.uuid
    source_type           = "image"
    volume_size           = 30
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = false
  }

  # Creates the networks according to input networks 
  dynamic "network" {
    for_each = toset([data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name])
    content {
      name = network.value
    }
  }

  
  # user_data script used
  user_data = file("${module.common_variables.ubuntu_1804.user_data_file}")
  # Do not replace the instance when the ssh key changes
  lifecycle {
    ignore_changes = [
      # Ignore changes to tags, e.g. because a management agent
      # updates these based on some ruleset managed elsewhere.
      key_pair, user_data, network
    ]
  }
}


resource "openstack_networking_secgroup_rule_v2" "secgroup_dataminer_publish_rule_1" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = join("/",[openstack_compute_instance_v2.dm_pool_manager_proto.network.0.fixed_ip_v4,"32"])
  security_group_id = openstack_networking_secgroup_v2.dataminer_publish.id
}

resource "openstack_networking_secgroup_rule_v2" "secgroup_dataminer_publish_rule_2" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 80
  port_range_max    = 80
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.dataminer_publish.id
}

resource "openstack_networking_secgroup_rule_v2" "secgroup_dataminer_publish_rule_3" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 443
  port_range_max    = 443
  remote_ip_prefix  = join("/",[openstack_compute_instance_v2.dm_pool_manager_proto.network.0.fixed_ip_v4,"32"])
  security_group_id = openstack_networking_secgroup_v2.dataminer_publish.id
}



module "instance_without_data_volume" {
  source = "../../modules/instance_without_data_volume"

  instances_without_data_volume_map = {
    dataminer_proto_ghost = {
      name              = "dataminer-proto-ghost",
      description       = "This instance is a DataMiner Ghost service",
      flavor            = module.common_variables.flavor_list.m1_large,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.shared_postgresql],
      security_groups   = [openstack_networking_secgroup_v2.dataminer_publish.name,data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      server_groups_ids = [],
      image_ref         = module.common_variables.ubuntu_1804
      image_volume_size = 50
    },
    dataminer_1_proto_1804 = {
      name              = "dataminer1-proto-1804",
      description       = "This instance is a DataMiner service",
      flavor            = module.common_variables.flavor_list.m1_xxl,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.shared_postgresql],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      server_groups_ids = [],
      image_ref         = module.common_variables.ubuntu_1804
      image_volume_size = 80
    },
    dataminer_2_proto_1804 = {
      name              = "dataminer2-proto-1804",
      description       = "This instance is a DataMiner service",
      flavor            = module.common_variables.flavor_list.m1_xxl,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.shared_postgresql],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      server_groups_ids = [],
      image_ref         = module.common_variables.ubuntu_1804
      image_volume_size = 80
    },
    dataminer_3_proto_1804 = {
      name              = "dataminer3-proto-1804",
      description       = "This instance is a DataMiner service",
      flavor            = module.common_variables.flavor_list.m1_xxl,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.shared_postgresql],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      server_groups_ids = [],
      image_ref         = module.common_variables.ubuntu_1804
      image_volume_size = 80
    },
    dataminer_4_proto_1804 = {
      name              = "dataminer4-proto-1804",
      description       = "This instance is a DataMiner service",
      flavor            = module.common_variables.flavor_list.m1_xxl,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.shared_postgresql],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      server_groups_ids = [],
      image_ref         = module.common_variables.ubuntu_1804
      image_volume_size = 80
    },
    dataminer_5_proto_1804 = {
      name              = "dataminer5-proto-1804",
      description       = "This instance is a DataMiner service",
      flavor            = module.common_variables.flavor_list.m1_xxl,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.shared_postgresql],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      server_groups_ids = [],
      image_ref         = module.common_variables.ubuntu_1804
      image_volume_size = 80
    },
    dataminer_6_proto_1804 = {
      name              = "dataminer6-proto-1804",
      description       = "This instance is a DataMiner service",
      flavor            = module.common_variables.flavor_list.m1_xxl,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.shared_postgresql],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      server_groups_ids = [],
      image_ref         = module.common_variables.ubuntu_1804
      image_volume_size = 80
    },
    dataminer_7_proto_1804 = {
      name              = "dataminer7-proto-1804",
      description       = "This instance is a DataMiner service",
      flavor            = module.common_variables.flavor_list.m1_xxl,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.shared_postgresql],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      server_groups_ids = [],
      image_ref         = module.common_variables.ubuntu_1804
      image_volume_size = 80
    },
    dataminer_8_proto_1804 = {
      name              = "dataminer8-proto-1804",
      description       = "This instance is a DataMiner service",
      flavor            = module.common_variables.flavor_list.m1_xxl,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.shared_postgresql],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      server_groups_ids = [],
      image_ref         = module.common_variables.ubuntu_1804
      image_volume_size = 80
    },
    dataminer_2_gw_proto_1804 = {
      name              = "dataminer2-gw-proto-1804",
      description       = "This instance is a DataMiner service",
      flavor            = module.common_variables.flavor_list.c2_large,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.shared_postgresql],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      server_groups_ids = [],
      image_ref         = module.common_variables.ubuntu_1804
      image_volume_size = 40
    },
    dataminer_3_gw_proto_1804 = {
      name              = "dataminer3-gw-proto-1804",
      description       = "This instance is a DataMiner service",
      flavor            = module.common_variables.flavor_list.c2_large,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.shared_postgresql],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      server_groups_ids = [],
      image_ref         = module.common_variables.ubuntu_1804
      image_volume_size = 40
    },
    dataminer_4_gw_proto_1804 = {
      name              = "dataminer4-gw-proto-1804",
      description       = "This instance is a DataMiner service",
      flavor            = module.common_variables.flavor_list.c2_large,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.shared_postgresql],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      server_groups_ids = [],
      image_ref         = module.common_variables.ubuntu_1804
      image_volume_size = 40
    },
    dataminer_5_gw_proto_1804 = {
      name              = "dataminer5-gw-proto-1804",
      description       = "This instance is a DataMiner service",
      flavor            = module.common_variables.flavor_list.c2_large,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.shared_postgresql],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      server_groups_ids = [],
      image_ref         = module.common_variables.ubuntu_1804
      image_volume_size = 40
    }
  }
}

locals {
  cname_target = "main-lb.${data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name}"
}

#
# Add DNS record/s
#
module "dns_records_create" {
  source = "../../modules/dns_resources"

  dns_resources_map = {
    dm-pool-manager-proto = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["dm-pool-manager-proto", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "DM Pool Manager Proto"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    dataminer-proto-ghost = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["dataminer-proto-ghost", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "DataMiner Proto Ghost"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    dataminer1-proto-1804 = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["dataminer1-proto-1804", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "DataMiner 1 Proto 1804"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    dataminer2-proto-1804 = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["dataminer2-proto-1804", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "DataMiner 2 Proto 1804"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    dataminer3-proto-1804 = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["dataminer3-proto-1804", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "DataMiner 3 Proto 1804"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    dataminer4-proto-1804 = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["dataminer4-proto-1804", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "DataMiner 4 Proto 1804"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    dataminer5-proto-1804 = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["dataminer5-proto-1804", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "DataMiner 5 Proto 1804"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    dataminer6-proto-1804 = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["dataminer6-proto-1804", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "DataMiner 6 Proto 1804"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    dataminer7-proto-1804 = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["dataminer7-proto-1804", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "DataMiner 7 Proto 1804"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    dataminer8-proto-1804 = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["dataminer8-proto-1804", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "DataMiner 8 Proto 1804"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    dataminer2-gw-proto-1804 = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["dataminer2-gw-proto-1804", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "DataMiner 2 Generic Worker Proto 1804"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    dataminer3-gw-proto-1804 = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["dataminer3-gw-proto-1804", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "DataMiner 3 Generic Worker Proto 1804"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    dataminer4-gw-proto-1804 = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["dataminer4-gw-proto-1804", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "DataMiner 4 Generic Worker Proto 1804"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    dataminer5-gw-proto-1804 = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["dataminer5-gw-proto-1804", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "DataMiner 5 Generic Worker Proto 1804"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    dataminer-prototypes = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["dataminer-prototypes", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "DataMiner dataminer-prototypes load balancer"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    dataminer-prototypes-workers = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["dataminer-prototypes-workers", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "DataMiner dataminer-prototypes-workers load balancer"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    }
  }
}
