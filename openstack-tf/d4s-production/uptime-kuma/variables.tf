# Uptime Kuma resources

variable "uptime_data" {
  type = map(string)
  default = {
    name   = "uptime-kuma"
    flavor = "d1.large"
  }
}
