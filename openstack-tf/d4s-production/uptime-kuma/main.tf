# Define required providers
terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = ">= 1.54.0"
    }
  }
}

data "terraform_remote_state" "privnet_dns_router" {
  backend = "local"

  config = {
    path = "../project-setup/terraform.tfstate"
  }
}

data "terraform_remote_state" "infrastructure_setup" {
  backend = "local"

  config = {
    path = "../basic-infrastructure/terraform.tfstate"
  }
}

#
# Uses common_variables as module
#
module "common_variables" {
  source = "../../modules/common_variables"
}

# Module used
module "ssh_settings" {
  source = "../../modules/ssh-key-ref"
}

# Instance volumes.
# One for the docker stuff in /var/lib/docker.
resource "openstack_blockstorage_volume_v3" "uptime_kuma_docker_volume" {
  name = "uptime-kuma-docker-vol"
  size = 100
}
# One for the data in /var/lib/uptime-kuma
resource "openstack_blockstorage_volume_v3" "uptime_kuma_server_volume" {
  name = "uptime-kuma-server-vol"
  size = 20
}

# Port in the main private network
resource "openstack_networking_port_v2" "uptime_kuma_server_port" {
  name               = "uptime-kuma-server-port"
  admin_state_up     = true
  network_id         = data.terraform_remote_state.privnet_dns_router.outputs.main_private_network_id
  security_group_ids = [data.terraform_remote_state.infrastructure_setup.outputs.default_security_group.id, "08da8012-6fa8-456f-bac5-0af25763182c" ]
}

# Instance
resource "openstack_compute_instance_v2" "uptime_kuma_server" {
  name                    = var.uptime_data.name
  availability_zone_hints = module.common_variables.availability_zone_no_gpu_name
  flavor_name             = var.uptime_data.flavor
  key_pair                = module.ssh_settings.ssh_key_name

  block_device {
    uuid                  = module.common_variables.ubuntu_2204.uuid
    source_type           = "image"
    volume_size           = 10
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = false
  }

  network {
    port = openstack_networking_port_v2.uptime_kuma_server_port.id
  }

  user_data = file("${module.common_variables.ubuntu2204_data_file}")
  # Do not replace the instance when the ssh key changes
  lifecycle {
    ignore_changes = [
      # Ignore changes to tags, e.g. because a management agent
      # updates these based on some ruleset managed elsewhere.
      key_pair, user_data, network
    ]
  }
}

# Attach the volumes to the instance
resource "openstack_compute_volume_attach_v2" "uptime_kuma_docker_volume_attach" {
  instance_id = openstack_compute_instance_v2.uptime_kuma_server.id
  volume_id   = openstack_blockstorage_volume_v3.uptime_kuma_docker_volume.id
  device      = "/dev/vdb"
}

resource "openstack_compute_volume_attach_v2" "uptime_kuma_server_volume_attach" {
  instance_id = openstack_compute_instance_v2.uptime_kuma_server.id
  volume_id   = openstack_blockstorage_volume_v3.uptime_kuma_server_volume.id
  device      = "/dev/vdc"
}

# Allocate a floating IP
resource "openstack_networking_floatingip_v2" "uptime_kuma_floating_ip" {
  pool        = data.terraform_remote_state.infrastructure_setup.outputs.floating_ip_pools.main_public_ip_pool
  description = "Public IP address for the Uptime Kuma Server"
}

# Associate the floating IP to the instance
resource "openstack_networking_floatingip_associate_v2" "uptime_kuma_ip" {
  floating_ip = openstack_networking_floatingip_v2.uptime_kuma_floating_ip.address
  port_id     = openstack_networking_port_v2.uptime_kuma_server_port.id
}

locals {
  uptime_kuma_recordset_name = "uptime-kuma.${data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name}"
}

resource "openstack_dns_recordset_v2" "uptime_kuma_recordset" {
  zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
  name        = local.uptime_kuma_recordset_name
  description = "Public IP address of the Uptime Kuma Server"
  ttl         = 8600
  type        = "A"
  records     = [openstack_networking_floatingip_v2.uptime_kuma_floating_ip.address]
}

output "uptime_kuma_public_ip_address" {
  value = openstack_networking_floatingip_v2.uptime_kuma_floating_ip.address
}

output "uptime_kuma_hostname" {
  value = openstack_dns_recordset_v2.uptime_kuma_recordset.name
}
