# D4Science website, drupal based
# mount point: web/sites/default/files
resource "openstack_sharedfilesystem_share_v2" "d4science_web_drupal_files" {
  name        = "d4science_web_drupal_files"
  description = "NFS share for the D4Science website (Drupal files)"
  share_proto = "NFS"
  size        = 5
}

output "d4science_web_drupal_files" {
  value = openstack_sharedfilesystem_share_v2.d4science_web_drupal_files
}

# Allow access to the NFS share
resource "openstack_sharedfilesystem_share_access_v2" "d4science_web_drupal_swarm_mgr_acl" {
  for_each     = { for nfs_ip in data.terraform_remote_state.main_infrastructure.outputs.swarm_managers_nfs_ip_ports : join("", nfs_ip.all_fixed_ips) => nfs_ip }
  share_id     = openstack_sharedfilesystem_share_v2.d4science_web_drupal_files.id
  access_type  = "ip"
  access_to    = each.key
  access_level = "rw"
}

output "d4science_web_drupal_swarm_mgr_acl" {
  value     = openstack_sharedfilesystem_share_access_v2.d4science_web_drupal_swarm_mgr_acl
  sensitive = true
}

resource "openstack_sharedfilesystem_share_access_v2" "d4science_web_drupal_swarm_workers_acl" {
  for_each     = { for nfs_ip in data.terraform_remote_state.main_infrastructure.outputs.swarm_workers_nfs_ip_ports : join("", nfs_ip.all_fixed_ips) => nfs_ip }
  share_id     = openstack_sharedfilesystem_share_v2.d4science_web_drupal_files.id
  access_type  = "ip"
  access_to    = each.key
  access_level = "rw"
}

output "d4science_web_drupal_swarm_workers_acl" {
  value     = openstack_sharedfilesystem_share_access_v2.d4science_web_drupal_swarm_workers_acl
  sensitive = true
}

# pgadmin instance
resource "openstack_sharedfilesystem_share_v2" "d4science_web_pgadmin_files" {
  name        = "d4science_web_pgadmin_files"
  description = "NFS share for the D4Science website (PGadmin data)"
  share_proto = "NFS"
  size        = 5
}

output "d4science_web_pgadmin_files" {
  value = openstack_sharedfilesystem_share_v2.d4science_web_pgadmin_files
}

# Allow access to the NFS share
resource "openstack_sharedfilesystem_share_access_v2" "d4science_web_pgadmin_swarm_mgr_acl" {
  for_each     = { for nfs_ip in data.terraform_remote_state.main_infrastructure.outputs.swarm_managers_nfs_ip_ports : join("", nfs_ip.all_fixed_ips) => nfs_ip }
  share_id     = openstack_sharedfilesystem_share_v2.d4science_web_pgadmin_files.id
  access_type  = "ip"
  access_to    = each.key
  access_level = "rw"
}

output "d4science_web_pgadmin_swarm_mgr_acl" {
  value     = openstack_sharedfilesystem_share_access_v2.d4science_web_pgadmin_swarm_mgr_acl
  sensitive = true
}

resource "openstack_sharedfilesystem_share_access_v2" "d4science_web_pgadmin_swarm_workers_acl" {
  for_each     = { for nfs_ip in data.terraform_remote_state.main_infrastructure.outputs.swarm_workers_nfs_ip_ports : join("", nfs_ip.all_fixed_ips) => nfs_ip }
  share_id     = openstack_sharedfilesystem_share_v2.d4science_web_pgadmin_files.id
  access_type  = "ip"
  access_to    = each.key
  access_level = "rw"
}

output "d4science_web_pgadmin_swarm_workers_acl" {
  value     = openstack_sharedfilesystem_share_access_v2.d4science_web_pgadmin_swarm_workers_acl
  sensitive = true
}

