# d4s_pgadmin_pgadmin_data
resource "openstack_sharedfilesystem_share_v2" "d4s_pgadmin_pgadmin" {
  name        = "d4s_pgadmin_pgadmin"
  description = "NFS share for the D4Science production pgAdmin instance"
  share_proto = "NFS"
  size        = 6
}

output "d4s_pgadmin_pgadmin" {
  value = openstack_sharedfilesystem_share_v2.d4s_pgadmin_pgadmin
}

# Allow access to the NFS share
resource "openstack_sharedfilesystem_share_access_v2" "d4s_pgadmin_pgadmin_swarm_mgr_acl" {
  for_each     = { for nfs_ip in data.terraform_remote_state.main_infrastructure.outputs.swarm_managers_nfs_ip_ports : join("", nfs_ip.all_fixed_ips) => nfs_ip }
  share_id     = openstack_sharedfilesystem_share_v2.d4s_pgadmin_pgadmin.id
  access_type  = "ip"
  access_to    = each.key
  access_level = "rw"
}

output "d4s_pgadmin_pgadmin_swarm_mgr_acl" {
  value     = openstack_sharedfilesystem_share_access_v2.d4s_pgadmin_pgadmin_swarm_mgr_acl
  sensitive = true
}

resource "openstack_sharedfilesystem_share_access_v2" "d4s_pgadmin_pgadmin_swarm_workers_acl" {
  for_each     = { for nfs_ip in data.terraform_remote_state.main_infrastructure.outputs.swarm_workers_nfs_ip_ports : join("", nfs_ip.all_fixed_ips) => nfs_ip }
  share_id     = openstack_sharedfilesystem_share_v2.d4s_pgadmin_pgadmin.id
  access_type  = "ip"
  access_to    = each.key
  access_level = "rw"
}

output "d4s_pgadmin_pgadmin_swarm_workers_acl" {
  value     = openstack_sharedfilesystem_share_access_v2.d4s_pgadmin_pgadmin_swarm_workers_acl
  sensitive = true
}
