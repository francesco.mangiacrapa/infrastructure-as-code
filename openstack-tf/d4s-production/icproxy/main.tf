# Define required providers
terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = ">= 1.54.0"
    }
  }
}

data "terraform_remote_state" "privnet_dns_router" {
  backend = "local"

  config = {
    path = "../project-setup/terraform.tfstate"
  }
}

#
# Uses common_variables as module
#
module "common_variables" {
  source = "../../modules/common_variables"
}

# Module used
module "ssh_settings" {
  source = "../../modules/ssh-key-ref"
}


# Instances
resource "openstack_compute_instance_v2" "icproxy1_prod" {
  name                    = "registry1"
  availability_zone_hints = module.common_variables.availability_zone_no_gpu_name
  flavor_name             = "m1.large"
  key_pair                = module.ssh_settings.ssh_key_name
  security_groups         = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers]
  block_device {
    uuid                  = module.common_variables.ubuntu_1804.uuid
    source_type           = "image"
    volume_size           = 10
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = false
  }

  # Creates the networks according to input networks 
  dynamic "network" {
    for_each = toset([data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name])
    content {
      name = network.value
    }
  }


  # user_data script used
  user_data = file("${module.common_variables.ubuntu_1804.user_data_file}")
  # Do not replace the instance when the ssh key changes
  lifecycle {
    ignore_changes = [
      # Ignore changes to tags, e.g. because a management agent
      # updates these based on some ruleset managed elsewhere.
      key_pair, user_data, network
    ]
  }
}

resource "openstack_compute_instance_v2" "icproxy2_prod" {
  name                    = "registry2"
  availability_zone_hints = module.common_variables.availability_zone_no_gpu_name
  flavor_name             = "m1.large"
  key_pair                = module.ssh_settings.ssh_key_name
  security_groups         = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers]
  block_device {
    uuid                  = module.common_variables.ubuntu_1804.uuid
    source_type           = "image"
    volume_size           = 10
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = false
  }

  # Creates the networks according to input networks 
  dynamic "network" {
    for_each = toset([data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name])
    content {
      name = network.value
    }
  }


  # user_data script used
  user_data = file("${module.common_variables.ubuntu_1804.user_data_file}")
  # Do not replace the instance when the ssh key changes
  lifecycle {
    ignore_changes = [
      # Ignore changes to tags, e.g. because a management agent
      # updates these based on some ruleset managed elsewhere.
      key_pair, user_data, network
    ]
  }
}


locals {
  cname_target = "main-lb.${data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name}"
}

#
# Add DNS record/s
#
module "dns_records_create" {
  source = "../../modules/dns_resources"

  dns_resources_map = {
    icproxy1-prod = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["registry1", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "ICProxy 1"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    icproxy2-prod = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["registry2", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "ICProxy 2"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    icproxy-prod = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["registry", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "ICProxy registry"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    }
  }
}

