# Main services

* Load balancer as a service (openstack), L4.

> * Main Octavia load balancer

* Two VMs as HAPROXY L7 instances for the main services. The dataminers will be also served by this load balancer.
* A shell server, with floating IP address, that will be used as a proxy to reach all the other VMs.
* A internal CA service.
* A Prometheus instance.
* A PostgreSQL server instance, with a dedicated network
