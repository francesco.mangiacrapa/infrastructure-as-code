# Block device for the backup partition
resource "openstack_blockstorage_volume_v3" "shared_postgresql_backup_vol" {
  name = var.shared_postgresql_server_data.vol_backup_name
  size = var.shared_postgresql_server_data.vol_backup_size
}

resource "openstack_compute_volume_attach_v2" "shared_postgresql_backup_attach_vol" {
  instance_id = openstack_compute_instance_v2.shared_postgresql_server.id
  volume_id   = openstack_blockstorage_volume_v3.shared_postgresql_backup_vol.id
  device      = var.shared_postgresql_server_data.vol_backup_device
  depends_on  = [openstack_compute_instance_v2.shared_postgresql_server]
}
