# Instance
resource "openstack_compute_instance_v2" "solr_ariadne" {
  name   = "solr-ariadne"
  availability_zone_hints = module.common_variables.availability_zone_no_gpu_name
  flavor_name             = module.common_variables.flavor_list.m1_large
  key_pair                = module.ssh_settings.ssh_key_name
  security_groups         = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers]
  block_device {
    uuid                  = module.common_variables.ubuntu_1804.uuid
    source_type           = "image"
    volume_size           = 100
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = false
  }

  # Creates the networks according to input networks 
  dynamic "network" {
    for_each = toset([data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name])
    content {
      name = network.value
    }
  }

  
  # user_data script used
  user_data = file("${module.common_variables.ubuntu_1804.user_data_file}")
  # Do not replace the instance when the ssh key changes
  lifecycle {
    ignore_changes = [
      # Ignore changes to tags, e.g. because a management agent
      # updates these based on some ruleset managed elsewhere.
      key_pair, user_data, network
    ]
  }
}
