# Define required providers
terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.53.0"
    }
  }
}

data "terraform_remote_state" "privnet_dns_router" {
  backend = "local"

  config = {
    path = "../project-setup/terraform.tfstate"
  }
}

module "liferay" {
  source = "../../modules/liferay"
  liferay_data = {
    affinity_policy = "anti-affinity"
    srv_name        = "lr62"
    vm_count        = 2
    vm_flavor       = "m2.medium"
    boot_vol_size   = 30
    share_description = "NFS share for the liferay production static data"
    share_name = "liferay_production_nfs_share"
  }

  liferay_ip_addrs = ["10.1.40.40", "10.1.40.41"]

  liferay_recordsets = {
    liferay_main_record = {
      name        = join(".", ["infra-gateway", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name]),
      description = "Liferay Production"
    },
    liferay_test_endpoint = {
      name        = join(".", ["osportal", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name]),
      description = "Liferay Production endpoint used to test the functionalities"
    }
  }
}
