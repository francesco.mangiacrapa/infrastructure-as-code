# Define required providers
terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.53.0"
    }
  }
}

data "terraform_remote_state" "privnet_dns_router" {
  backend = "local"

  config = {
    path = "../project-setup/terraform.tfstate"
  }
}

#
# Uses common_variables as module
#
module "common_variables" {
  source = "../../modules/common_variables"
}

# Module used
module "ssh_settings" {
  source = "../../modules/ssh-key-ref"
}

# SSH access from everywhere
resource "openstack_networking_secgroup_v2" "static_web_ssh_from_outside" {
  name                 = "static_web_ssh_from_outside"
  delete_default_rules = "true"
  description          = "Access the static web VM from the Internet"
}

resource "openstack_networking_secgroup_rule_v2" "static_web_public_ssh_access" {
  security_group_id = openstack_networking_secgroup_v2.static_web_ssh_from_outside.id
  description       = "Access the static web VM from the Internet"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = "0.0.0.0/0"
}
# Instances
resource "openstack_compute_instance_v2" "static_web_service" {
  name                    = "static-web"
  availability_zone_hints = module.common_variables.availability_zone_no_gpu_name
  flavor_name             = module.common_variables.flavor_list.c1_small
  key_pair                = module.ssh_settings.ssh_key_name
  security_groups         = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers, openstack_networking_secgroup_v2.static_web_ssh_from_outside.name]
  block_device {
    uuid                  = module.common_variables.ubuntu_2204.uuid
    source_type           = "image"
    volume_size           = 40
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = false
  }

  # Creates the networks according to input networks 
  dynamic "network" {
    for_each = toset([data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name])
    content {
      name = network.value
    }
  }

  # user_data script used
  user_data = file("${module.common_variables.ubuntu_2204.user_data_file}")
  # Do not replace the instance when the ssh key changes
  lifecycle {
    ignore_changes = [
      # Ignore changes to tags, e.g. because a management agent
      # updates these based on some ruleset managed elsewhere.
      key_pair, user_data, network
    ]
  }
}

# Allocate a floating IP
resource "openstack_networking_floatingip_v2" "static_web_floating_ip" {
  pool = data.terraform_remote_state.privnet_dns_router.outputs.floating_ip_pools.main_public_ip_pool
  # The DNS association does not work because of a bug in the OpenStack API
  # dns_name = "main-lb"
  # dns_domain = var.dns_zone.zone_name
  description = "Static Web VM public IP address"
}

resource "openstack_compute_floatingip_associate_v2" "static_web_ip" {
  floating_ip = openstack_networking_floatingip_v2.static_web_floating_ip.address
  instance_id = openstack_compute_instance_v2.static_web_service.id
  depends_on  = [openstack_networking_floatingip_v2.static_web_floating_ip]
}

resource "openstack_dns_recordset_v2" "static_web_dns_recordset" {
  zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
  name        = join(".", ["static-web", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
  description = "Static Web Hostname"
  ttl         = 8600
  type        = "A"
  records     = [openstack_networking_floatingip_v2.static_web_floating_ip.address]
  depends_on  = [openstack_networking_floatingip_v2.static_web_floating_ip]
}

