# Terraform with Google Cloud

Reference: <https://registry.terraform.io/providers/hashicorp/google/latest/docs/guides/provider_reference>

* The authentication is managed using the google cloud cli, with `gcloud auth default login`.

It is also possible to use a service account file, pointing the `GOOGLE_APPLICATION_CREDENTIALS` environment variable to the path of the json file.
