variable "orientdb_data" {
  type = map(string)
  default = {
    node_name             = "orientdb"
    node_data_disk_size   = 10
    node_data_disk_device = "/dev/vdb"
  }
}

variable "orientdb_affinity_policy" {
  default = "soft-anti-affinity"
}
variable "orientdb_node_flavor" {
  default = ""
}

variable "orientdb_nodes_count" {
  default = ""
}

variable "orientdb_image_name" {
  default = ""
}
variable "orientdb_se_image_name" {
  default = ""
}

variable "orientdb_image_uuid" {
  default = ""
}
variable "orientdb_se_image_uuid" {
  default = ""
}

variable "orientdb_ip" {
  type    = list(string)
  default = ["192.168.10.5", "192.168.10.6", "192.168.10.7"]
}

variable "orientdb_cidr" {
  type    = list(string)
  default = ["192.168.10.5/32", "192.168.10.6/32", "192.168.10.7/32"]
}

variable "orientdb_se_node_flavor" {
  default = ""
}

variable "orientdb_se_ip" {
  default = ""
}
variable "orientdb_se_cidr" {
  default = ""
}

variable "orientdb_net" {
  type = map(string)
  default = {
    network_name          = "orientdb-net"
    network_description   = "Network used by the OrientDB cluster and to access the service"
    network_cidr          = "192.168.10.0/24"
    allocation_pool_start = "192.168.10.11"
    allocation_pool_end   = "192.168.10.254"
  }
}

variable "orientdb_se_net" {
  type = map(string)
  default = {
    network_name          = "orientdb-se-net"
    network_description   = "Network used by the OrientDB for Smart Executor"
    network_cidr          = "192.168.12.0/24"
    allocation_pool_start = "192.168.12.11"
    allocation_pool_end   = "192.168.12.254"
  }
}

variable "orientdb_se_secgroup" {
  default = "access_to_orientdb_se"
}

variable "postgresql_secgroup" {
  default = "PostgreSQL service"
}

