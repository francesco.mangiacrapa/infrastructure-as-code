variable "ubuntu_2204" {
  type = map(string)
  default = {
    name           = "Ubuntu 22.04 - GARR"
    uuid           = "94618f26-de42-4b1a-80a0-a88b73391a0a"
    user_data_file = "../../openstack_vm_data_scripts/ubuntu2204.sh"
  }
}

variable "ubuntu2204_data_file" {
  default = "../../openstack_vm_data_scripts/ubuntu2204.sh"
}

variable "ubuntu_2404" {
  type = map(string)
  default = {
    name           = "Ubuntu 24.04 - GARR"
    uuid           = "b2f00e68-3847-4f48-a8a6-b28012d57095"
    user_data_file = "../../openstack_vm_data_scripts/ubuntu2404.sh"
  }
}

variable "ubuntu2404_data_file" {
  default = "../../openstack_vm_data_scripts/ubuntu2404.sh"
}
# Create in the path 'modules/ssh-key-ref' the file 'ssh-key-ref-outputs.tf'
# with the following outputs:

# output "ssh_key_file" {
#   value     = "~/.ssh/{YOUR_PRIVATE_KEYNAME}"
#   sensitive = true
# }

# output "ssh_key_name" {
#   value     = "{YOUR_KEYNAME}"
#   sensitive = false
# }

# Then you can use above outputs in your 'file.tf' (if it contains the soft link to variables.tf) as:
#  module.ssh_settings.ssh_key_file
#  module.ssh_settings.ssh_key_name

variable "ssh_sources" {
  type = map(string)
  default = {
    s2i2s_vpn_1_cidr      = "146.48.28.10/32"
    s2i2s_vpn_2_cidr      = "146.48.28.11/32"
    d4s_vpn_1_cidr        = "146.48.122.27/32"
    d4s_vpn_2_cidr        = "146.48.122.49/32"
    shell_d4s_cidr        = "146.48.122.95/32"
    infrascience_net_cidr = "146.48.122.0/23"
  }
}

