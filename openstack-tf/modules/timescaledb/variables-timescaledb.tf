variable "timescaledb_server_data" {
  type = map(string)
  default = {
    node_name             = "timescaledb"
    node_data_disk_size   = 20
    node_data_disk_device = "/dev/vdb"
  }
}

variable "timescaledb_affinity_policy" {
  default = "soft-anti-affinity"
}

variable "timescaledb_node_flavor" {
  default = ""
}

variable "timescaledb_nodes_count" {
  default = 0
}

variable "timescaledb_ip" {
  type    = list(string)
  default = []
}

variable "timescaledb_cidr" {
  type    = list(string)
  default = []
}

variable "timescaledb_net" {
  type = map(string)
  default = {
    network_name          = "timescaledb-net"
    network_description   = "Network used by the Timescaledb cluster and to access the service"
    network_cidr          = "192.168.11.0/24"
    allocation_pool_start = "192.168.11.20"
    allocation_pool_end   = "192.168.11.254"
  }
}
