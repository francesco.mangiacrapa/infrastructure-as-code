output "timescaledb_net" {
  value = openstack_networking_network_v2.timescaledb_net
}

output "timescaledb_subnet" {
  value = openstack_networking_subnet_v2.timescaledb_subnet
}

output "timescaledb_servers" {
  value = openstack_compute_instance_v2.timescaledb_server
}
