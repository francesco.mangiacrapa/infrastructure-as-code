# Promertheus server. A floating IP is required
resource "openstack_blockstorage_volume_v3" "prometheus_data_vol" {
  name = var.prometheus_server_data.vol_data_name
  size = var.prometheus_server_data.vol_data_size
}

resource "openstack_compute_instance_v2" "prometheus_server" {
  name                    = var.prometheus_server_data.name
  availability_zone_hints = var.availability_zones_names.availability_zone_no_gpu
  flavor_name             = var.prometheus_server_data.flavor
  key_pair                = module.ssh_settings.ssh_key_name
  security_groups         = [var.default_security_group_name, openstack_networking_secgroup_v2.restricted_web.name, openstack_networking_secgroup_v2.prometheus_access_from_grafana.name]
  block_device {
    uuid                  = var.ubuntu_2204.uuid
    source_type           = "image"
    volume_size           = 10
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = false
  }

  network {
    name        = var.main_private_network.name
    fixed_ip_v4 = var.basic_services_ip.prometheus
  }
  user_data = file("${var.ubuntu2204_data_file}")
  # Do not replace the instance when the ssh key changes
  lifecycle {
    ignore_changes = [
      # Ignore changes to tags, e.g. because a management agent
      # updates these based on some ruleset managed elsewhere.
      key_pair, user_data, network
    ]
  }

}

resource "openstack_compute_volume_attach_v2" "prometheus_data_attach_vol" {
  instance_id = openstack_compute_instance_v2.prometheus_server.id
  volume_id   = openstack_blockstorage_volume_v3.prometheus_data_vol.id
  device      = var.prometheus_server_data.vol_data_device
}

# Floating IP and DNS record
resource "openstack_networking_floatingip_v2" "prometheus_server_ip" {
  pool = var.floating_ip_pools.main_public_ip_pool
  # The DNS association does not work because of a bug in the OpenStack API
  description = "Prometheus server"
}

resource "openstack_compute_floatingip_associate_v2" "prometheus_server" {
  floating_ip = openstack_networking_floatingip_v2.prometheus_server_ip.address
  instance_id = openstack_compute_instance_v2.prometheus_server.id
}

locals {
  prometheus_recordset_name   = "${var.prometheus_server_data.name}.${var.dns_zone.zone_name}"
  alertmanager_recordset_name = "alertmanager.${var.dns_zone.zone_name}"
}

resource "openstack_dns_recordset_v2" "prometheus_server_recordset" {
  zone_id     = var.dns_zone_id
  name        = local.prometheus_recordset_name
  description = "Public IP address of the Prometheus server"
  ttl         = 8600
  type        = "A"
  records     = [openstack_networking_floatingip_v2.prometheus_server_ip.address]
}

resource "openstack_dns_recordset_v2" "alertmanager_server_recordset" {
  zone_id     = var.dns_zone_id
  name        = local.alertmanager_recordset_name
  description = "Prometheus alertmanager"
  ttl         = 8600
  type        = "CNAME"
  records     = [local.prometheus_recordset_name]
}

output "prometheus_public_ip_address" {
  value = openstack_networking_floatingip_v2.prometheus_server_ip.address
}

output "prometheus_server_hostname" {
  value = openstack_dns_recordset_v2.prometheus_server_recordset.name
}

