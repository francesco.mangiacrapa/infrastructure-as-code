#
# This is the security group that should be added to every instance
resource "openstack_networking_secgroup_v2" "default" {
  name                 = var.default_security_group_name
  delete_default_rules = "true"
  description          = "Default security group with rules for ssh access via jump proxy, prometheus scraping. Permit limited egress traffic."
}

resource "openstack_networking_secgroup_rule_v2" "egress-icmp" {
  security_group_id = openstack_networking_secgroup_v2.default.id
  description       = "Allow ICMP to remote"
  direction         = "egress"
  ethertype         = "IPv4"
  remote_ip_prefix  = "0.0.0.0/0"
  protocol          = "icmp"
}

# resource "openstack_networking_secgroup_rule_v2" "egress-ipv4" {
#   security_group_id = openstack_networking_secgroup_v2.default.id
#   direction         = "egress"
#   ethertype         = "IPv4"
# }

resource "openstack_networking_secgroup_rule_v2" "egress-http" {
  security_group_id = openstack_networking_secgroup_v2.default.id
  direction         = "egress"
  description       = "egress HTTP"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 80
  port_range_max    = 80
  remote_ip_prefix  = "0.0.0.0/0"
}

resource "openstack_networking_secgroup_rule_v2" "egress-https" {
  security_group_id = openstack_networking_secgroup_v2.default.id
  direction         = "egress"
  description       = "egress HTTPS"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 443
  port_range_max    = 443
  remote_ip_prefix  = "0.0.0.0/0"
}

resource "openstack_networking_secgroup_rule_v2" "egress-ssh" {
  security_group_id = openstack_networking_secgroup_v2.default.id
  direction         = "egress"
  description       = "egress SSH"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = "0.0.0.0/0"
}

resource "openstack_networking_secgroup_rule_v2" "egress-ftp" {
  security_group_id = openstack_networking_secgroup_v2.default.id
  direction         = "egress"
  description       = "egress FTP"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 21
  port_range_max    = 21
  remote_ip_prefix  = "0.0.0.0/0"
}

resource "openstack_networking_secgroup_rule_v2" "egress-smtp-submission" {
  security_group_id = openstack_networking_secgroup_v2.default.id
  direction         = "egress"
  description       = "egress SMTP submission"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 587
  port_range_max    = 587
  remote_ip_prefix  = "146.48.0.0/16"
}

resource "openstack_networking_secgroup_rule_v2" "egress-imap" {
  security_group_id = openstack_networking_secgroup_v2.default.id
  direction         = "egress"
  description       = "egress IMAP"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 143
  port_range_max    = 143
  remote_ip_prefix  = "146.48.0.0/16"
}

resource "openstack_networking_secgroup_rule_v2" "egress-imaps" {
  security_group_id = openstack_networking_secgroup_v2.default.id
  direction         = "egress"
  description       = "egress IMAP"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 993
  port_range_max    = 993
  remote_ip_prefix  = "146.48.0.0/16"
}

resource "openstack_networking_secgroup_rule_v2" "egress-dns-udp" {
  security_group_id = openstack_networking_secgroup_v2.default.id
  direction         = "egress"
  description       = "egress DNS udp"
  ethertype         = "IPv4"
  protocol          = "udp"
  port_range_min    = 53
  port_range_max    = 53
  remote_ip_prefix  = "0.0.0.0/0"
}

resource "openstack_networking_secgroup_rule_v2" "egress-dns-tcp" {
  security_group_id = openstack_networking_secgroup_v2.default.id
  direction         = "egress"
  description       = "egress DNS tcp"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 53
  port_range_max    = 53
  remote_ip_prefix  = "0.0.0.0/0"
}

resource "openstack_networking_secgroup_rule_v2" "egress-ldap" {
  security_group_id = openstack_networking_secgroup_v2.default.id
  direction         = "egress"
  description       = "egress LDAP"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 389
  port_range_max    = 389
  remote_ip_prefix  = "0.0.0.0/0"
}

resource "openstack_networking_secgroup_rule_v2" "egress-ldaps" {
  security_group_id = openstack_networking_secgroup_v2.default.id
  direction         = "egress"
  description       = "egress LDAPs"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 636
  port_range_max    = 636
  remote_ip_prefix  = "0.0.0.0/0"
}

resource "openstack_networking_secgroup_rule_v2" "egress-ntp-udp" {
  security_group_id = openstack_networking_secgroup_v2.default.id
  direction         = "egress"
  description       = "egress NTP"
  ethertype         = "IPv4"
  protocol          = "udp"
  port_range_min    = 123
  port_range_max    = 123
  remote_ip_prefix  = "0.0.0.0/0"
}

resource "openstack_networking_secgroup_rule_v2" "egress-rpcbind-tcp" {
  security_group_id = openstack_networking_secgroup_v2.default.id
  direction         = "egress"
  description       = "egress rpcbind tcp"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 111
  port_range_max    = 111
  remote_ip_prefix  = "0.0.0.0/0"
}

resource "openstack_networking_secgroup_rule_v2" "egress-rpcbind-udp" {
  security_group_id = openstack_networking_secgroup_v2.default.id
  direction         = "egress"
  description       = "egress rpcbind udp"
  ethertype         = "IPv4"
  protocol          = "udp"
  port_range_min    = 111
  port_range_max    = 111
  remote_ip_prefix  = "0.0.0.0/0"
}

resource "openstack_networking_secgroup_rule_v2" "egress-bootps-udp" {
  security_group_id = openstack_networking_secgroup_v2.default.id
  direction         = "egress"
  description       = "egress bootps"
  ethertype         = "IPv4"
  protocol          = "udp"
  port_range_min    = 67
  port_range_max    = 67
}

resource "openstack_networking_secgroup_rule_v2" "ingress-bootpc-udp" {
  security_group_id = openstack_networking_secgroup_v2.default.id
  direction         = "ingress"
  description       = "ingress bootpc"
  ethertype         = "IPv4"
  protocol          = "udp"
  port_range_min    = 68
  port_range_max    = 68
}

resource "openstack_networking_secgroup_rule_v2" "egress-metadata-service" {
  security_group_id = openstack_networking_secgroup_v2.default.id
  direction         = "egress"
  description       = "egress metadata service"
  ethertype         = "IPv4"
  protocol          = "tcp"
  remote_ip_prefix  = "169.254.169.254"
}

resource "openstack_networking_secgroup_rule_v2" "egress-unprivileged-tcp" {
  security_group_id = openstack_networking_secgroup_v2.default.id
  direction         = "egress"
  description       = "egress unprivileged ports tcp"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 1024
  port_range_max    = 65535
  remote_ip_prefix  = "0.0.0.0/0"
}

resource "openstack_networking_secgroup_rule_v2" "egress-unprivileged-udp" {
  security_group_id = openstack_networking_secgroup_v2.default.id
  direction         = "egress"
  description       = "egress unprivileged ports udp"
  ethertype         = "IPv4"
  protocol          = "udp"
  port_range_min    = 1024
  port_range_max    = 65535
  remote_ip_prefix  = "0.0.0.0/0"
}

resource "openstack_networking_secgroup_rule_v2" "ingress-icmp" {
  security_group_id = openstack_networking_secgroup_v2.default.id
  description       = "Allow ICMP from remote"
  direction         = "ingress"
  ethertype         = "IPv4"
  remote_ip_prefix  = "0.0.0.0/0"
  protocol          = "icmp"
}

resource "openstack_networking_secgroup_rule_v2" "ssh-jump-proxy" {
  security_group_id = openstack_networking_secgroup_v2.default.id
  description       = "SSH traffic from the jump proxy"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = var.basic_services_ip.ssh_jump_cidr
}

resource "openstack_networking_secgroup_rule_v2" "prometheus-node" {
  security_group_id = openstack_networking_secgroup_v2.default.id
  description       = "Prometheus access to the node exporter"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 9100
  port_range_max    = 9100
  remote_ip_prefix  = var.basic_services_ip.prometheus_cidr
}

#
# SSH access to the jump proxy. Used by the jump proxy VM only
resource "openstack_networking_secgroup_v2" "access_to_the_jump_proxy" {
  name                 = "ssh_access_to_the_jump_node"
  delete_default_rules = "true"
  description          = "Security group that allows SSH access to the jump node from a limited set of sources"
}

resource "openstack_networking_secgroup_rule_v2" "ssh-s2i2s-vpn-1" {
  security_group_id = openstack_networking_secgroup_v2.access_to_the_jump_proxy.id
  description       = "SSH traffic from S2I2S VPN 1"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = var.ssh_sources.s2i2s_vpn_1_cidr
}

resource "openstack_networking_secgroup_rule_v2" "ssh-s2i2s-vpn-2" {
  security_group_id = openstack_networking_secgroup_v2.access_to_the_jump_proxy.id
  description       = "SSH traffic from S2I2S VPN 2"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = var.ssh_sources.s2i2s_vpn_2_cidr
}

resource "openstack_networking_secgroup_rule_v2" "ssh-d4s-vpn-1" {
  security_group_id = openstack_networking_secgroup_v2.access_to_the_jump_proxy.id
  description       = "SSH traffic from D4Science VPN 1"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = var.ssh_sources.d4s_vpn_1_cidr
}

resource "openstack_networking_secgroup_rule_v2" "ssh-d4s-vpn-2" {
  security_group_id = openstack_networking_secgroup_v2.access_to_the_jump_proxy.id
  description       = "SSH traffic from D4Science VPN 2"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = var.ssh_sources.d4s_vpn_2_cidr
}

resource "openstack_networking_secgroup_rule_v2" "ssh-shell-d4s" {
  security_group_id = openstack_networking_secgroup_v2.access_to_the_jump_proxy.id
  description       = "SSH traffic from shell.d4science.org"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = var.ssh_sources.shell_d4s_cidr
}

resource "openstack_networking_secgroup_rule_v2" "ssh-infrascience-net" {
  security_group_id = openstack_networking_secgroup_v2.access_to_the_jump_proxy.id
  description       = "SSH traffic from the InfraScience network"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = var.ssh_sources.infrascience_net_cidr
}

# Debug via tunnel from the jump proxy node
resource "openstack_networking_secgroup_v2" "debugging" {
  name                 = "debugging_from_jump_node"
  delete_default_rules = "true"
  description          = "Security group that allows web app debugging via tunnel from the ssh jump node"
}

resource "openstack_networking_secgroup_rule_v2" "shell_8100" {
  security_group_id = openstack_networking_secgroup_v2.debugging.id
  description       = "Tomcat debug on port 8100 from the shell jump proxy"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 8100
  port_range_max    = 8100
  remote_ip_prefix  = var.basic_services_ip.ssh_jump_cidr
}

resource "openstack_networking_secgroup_rule_v2" "shell_80" {
  security_group_id = openstack_networking_secgroup_v2.debugging.id
  description       = "http debug port 80 from the shell jump proxy"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 80
  port_range_max    = 80
  remote_ip_prefix  = var.basic_services_ip.ssh_jump_cidr
}

resource "openstack_networking_secgroup_rule_v2" "shell_443" {
  security_group_id = openstack_networking_secgroup_v2.debugging.id
  description       = "https debug port 443 from the shell jump proxy"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 443
  port_range_max    = 443
  remote_ip_prefix  = var.basic_services_ip.ssh_jump_cidr
}

# Traffic from the main HAPROXY load balancers
# Use on the web services that are exposed through the main HAPROXY
resource "openstack_networking_secgroup_v2" "traffic_from_main_haproxy" {
  name                 = "traffic_from_the_main_load_balancers"
  delete_default_rules = "true"
  description          = "Allow traffic from the main L7 HAPROXY load balancers"
}

resource "openstack_networking_secgroup_rule_v2" "haproxy-l7-1-80" {
  security_group_id = openstack_networking_secgroup_v2.traffic_from_main_haproxy.id
  description       = "HTTP traffic from HAPROXY L7 1"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 80
  port_range_max    = 80
  remote_ip_prefix  = var.basic_services_ip.haproxy_l7_1_cidr
}

resource "openstack_networking_secgroup_rule_v2" "haproxy-l7-2-80" {
  security_group_id = openstack_networking_secgroup_v2.traffic_from_main_haproxy.id
  description       = "HTTP traffic from HAPROXY L7 2"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 80
  port_range_max    = 80
  remote_ip_prefix  = var.basic_services_ip.haproxy_l7_2_cidr
}

resource "openstack_networking_secgroup_rule_v2" "haproxy-l7-1-443" {
  security_group_id = openstack_networking_secgroup_v2.traffic_from_main_haproxy.id
  description       = "HTTPS traffic from HAPROXY L7 1"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 443
  port_range_max    = 443
  remote_ip_prefix  = var.basic_services_ip.haproxy_l7_1_cidr
}

resource "openstack_networking_secgroup_rule_v2" "haproxy-l7-2-443" {
  security_group_id = openstack_networking_secgroup_v2.traffic_from_main_haproxy.id
  description       = "HTTPS traffic from HAPROXY L7 2"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 443
  port_range_max    = 443
  remote_ip_prefix  = var.basic_services_ip.haproxy_l7_2_cidr
}

resource "openstack_networking_secgroup_rule_v2" "haproxy-l7-1-8080" {
  security_group_id = openstack_networking_secgroup_v2.traffic_from_main_haproxy.id
  description       = "HTTP traffic from HAPROXY L7 1"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 8080
  port_range_max    = 8080
  remote_ip_prefix  = var.basic_services_ip.haproxy_l7_1_cidr
}

resource "openstack_networking_secgroup_rule_v2" "haproxy-l7-2-8080" {
  security_group_id = openstack_networking_secgroup_v2.traffic_from_main_haproxy.id
  description       = "HTTP traffic from HAPROXY L7 2"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 8080
  port_range_max    = 8080
  remote_ip_prefix  = var.basic_services_ip.haproxy_l7_2_cidr
}

resource "openstack_networking_secgroup_rule_v2" "haproxy-l7-1-8888" {
  security_group_id = openstack_networking_secgroup_v2.traffic_from_main_haproxy.id
  description       = "HTTP traffic from HAPROXY L7 1"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 8888
  port_range_max    = 8888
  remote_ip_prefix  = var.basic_services_ip.haproxy_l7_1_cidr
}

resource "openstack_networking_secgroup_rule_v2" "haproxy-l7-2-8888" {
  security_group_id = openstack_networking_secgroup_v2.traffic_from_main_haproxy.id
  description       = "HTTP traffic from HAPROXY L7 2"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 8888
  port_range_max    = 8888
  remote_ip_prefix  = var.basic_services_ip.haproxy_l7_2_cidr
}

# Security group that exposes web services directly. A floating IP is required.
resource "openstack_networking_secgroup_v2" "public_web" {
  name                 = "public_web_service"
  delete_default_rules = "true"
  description          = "Security group that allows HTTPS and HTTP from everywhere, for the services that are not behind any load balancer"
}

resource "openstack_networking_secgroup_rule_v2" "public_http" {
  security_group_id = openstack_networking_secgroup_v2.public_web.id
  description       = "Allow HTTP from everywhere"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 80
  port_range_max    = 80
  remote_ip_prefix  = "0.0.0.0/0"
}

resource "openstack_networking_secgroup_rule_v2" "public_https" {
  security_group_id = openstack_networking_secgroup_v2.public_web.id
  description       = "Allow HTTPS from everywhere"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 443
  port_range_max    = 443
  remote_ip_prefix  = "0.0.0.0/0"
}

# HTTP and HTTPS access through the VPN nodes. Floating IP is required
resource "openstack_networking_secgroup_v2" "restricted_web" {
  name                 = "restricted_web_service"
  delete_default_rules = "true"
  description          = "Security group that restricts HTTPS sources to the VPN nodes and shell.d4science.org. HTTP is open to all, because letsencrypt"
}

resource "openstack_networking_secgroup_rule_v2" "http_from_everywhere" {
  security_group_id = openstack_networking_secgroup_v2.restricted_web.id
  description       = "Allow HTTP from everywhere"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 80
  port_range_max    = 80
  remote_ip_prefix  = "0.0.0.0/0"
}

resource "openstack_networking_secgroup_rule_v2" "https_from_d4s_vpn_1" {
  security_group_id = openstack_networking_secgroup_v2.restricted_web.id
  description       = "Allow HTTPS from D4Science VPN 1"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 443
  port_range_max    = 443
  remote_ip_prefix  = var.ssh_sources.d4s_vpn_1_cidr
}

resource "openstack_networking_secgroup_rule_v2" "https_from_d4s_vpn_2" {
  security_group_id = openstack_networking_secgroup_v2.restricted_web.id
  description       = "Allow HTTPS from D4Science VPN 2"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 443
  port_range_max    = 443
  remote_ip_prefix  = var.ssh_sources.d4s_vpn_2_cidr
}

resource "openstack_networking_secgroup_rule_v2" "https_from_s2i2s_vpn_1" {
  security_group_id = openstack_networking_secgroup_v2.restricted_web.id
  description       = "Allow HTTPS from S2I2S VPN 1"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 443
  port_range_max    = 443
  remote_ip_prefix  = var.ssh_sources.s2i2s_vpn_1_cidr
}

resource "openstack_networking_secgroup_rule_v2" "https_from_s2i2s_vpn_2" {
  security_group_id = openstack_networking_secgroup_v2.restricted_web.id
  description       = "Allow HTTPS from S2I2S VPN 2"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 443
  port_range_max    = 443
  remote_ip_prefix  = var.ssh_sources.s2i2s_vpn_2_cidr
}

resource "openstack_networking_secgroup_rule_v2" "https_from_shell_d4s" {
  security_group_id = openstack_networking_secgroup_v2.restricted_web.id
  description       = "Allow HTTPS from shell.d4science.org"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 443
  port_range_max    = 443
  remote_ip_prefix  = var.ssh_sources.shell_d4s_cidr
}

resource "openstack_networking_secgroup_v2" "prometheus_access_from_grafana" {
  name                 = "prometheus_access_from_grafana"
  delete_default_rules = "true"
  description          = "The public grafana server must be able to get data from Prometheus"
}

resource "openstack_networking_secgroup_rule_v2" "grafana_d4s" {
  security_group_id = openstack_networking_secgroup_v2.prometheus_access_from_grafana.id
  description       = "Allow HTTPS from grafana.d4science.org"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 443
  port_range_max    = 443
  remote_ip_prefix  = var.prometheus_server_data.public_grafana_server_cidr
}

resource "openstack_networking_secgroup_rule_v2" "main_network_to_prometheus" {
  security_group_id = openstack_networking_secgroup_v2.prometheus_access_from_grafana.id
  description       = "Allow HTTPS from the main network"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 443
  port_range_max    = 443
  #   remote_ip_prefix  = data.terraform_remote_state.privnet_dns_router.outputs.external_gateway_ip.ip_address
  remote_ip_prefix = "146.48.28.0/22"
}

output "default_security_group" {
  value = openstack_networking_secgroup_v2.default
}

