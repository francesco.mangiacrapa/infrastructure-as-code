# Main load balancer. L4, backed by Octavia
resource "openstack_lb_loadbalancer_v2" "main_lb" {
  vip_subnet_id = var.main_private_subnet_id
  name          = var.octavia_information.main_lb_name
  description   = var.octavia_information.main_lb_description
  flavor_id     = var.octavia_information.octavia_flavor_id
  vip_address   = var.basic_services_ip.octavia_main
  # availability_zone     = var.availability_zones_names.availability_zone_no_gpu
  loadbalancer_provider = "amphora"
}

# Allocate a floating IP
resource "openstack_networking_floatingip_v2" "main_lb_ip" {
  pool = var.floating_ip_pools.main_public_ip_pool
  # The DNS association does not work because of a bug in the OpenStack API
  # dns_name = "main-lb"
  # dns_domain = var.dns_zone.zone_name
  description = var.octavia_information.main_lb_description
}

resource "openstack_networking_floatingip_associate_v2" "main_lb" {
  floating_ip = openstack_networking_floatingip_v2.main_lb_ip.address
  port_id     = openstack_lb_loadbalancer_v2.main_lb.vip_port_id
}

locals {
  recordset_name = "${var.octavia_information.main_lb_hostname}.${var.dns_zone.zone_name}"
}

resource "openstack_dns_recordset_v2" "main_lb_dns_recordset" {
  zone_id     = var.dns_zone_id
  name        = local.recordset_name
  description = "Public IP address of the main load balancer"
  ttl         = 8600
  type        = "A"
  records     = [openstack_networking_floatingip_v2.main_lb_ip.address]
}

# Main HAPROXY stats listener
resource "openstack_lb_listener_v2" "main_haproxy_stats_listener" {
  loadbalancer_id = openstack_lb_loadbalancer_v2.main_lb.id
  protocol        = "TCP"
  protocol_port   = 8880
  description     = "Listener for the stats of the main HAPROXY instances"
  name            = "main_haproxy_stats_listener"
  allowed_cidrs   = [var.ssh_sources.d4s_vpn_1_cidr, var.ssh_sources.d4s_vpn_2_cidr, var.ssh_sources.s2i2s_vpn_1_cidr, var.ssh_sources.s2i2s_vpn_2_cidr]

}

resource "openstack_lb_pool_v2" "main_haproxy_stats_pool" {
  listener_id = openstack_lb_listener_v2.main_haproxy_stats_listener.id
  protocol    = "TCP"
  lb_method   = "LEAST_CONNECTIONS"
  name        = "main-haproxy-lb-stats"
  description = "Pool for the stats of the main HAPROXY instances"
  persistence {
    type = "SOURCE_IP"
  }
}

resource "openstack_lb_members_v2" "main_haproxy_stats_pool_members" {
  pool_id = openstack_lb_pool_v2.main_haproxy_stats_pool.id
  member {
    name          = "haproxy l7 1"
    address       = var.basic_services_ip.haproxy_l7_1
    protocol_port = 8880
  }
  member {
    name          = "haproxy l7 2"
    address       = var.basic_services_ip.haproxy_l7_2
    protocol_port = 8880
  }
}

resource "openstack_lb_monitor_v2" "main_haproxy_stats_monitor" {
  pool_id        = openstack_lb_pool_v2.main_haproxy_stats_pool.id
  name           = "main_haproxy_stats_monitor"
  type           = "TCP"
  delay          = 20
  timeout        = 5
  max_retries    = 3
  admin_state_up = true
}

# Main HAPROXY HTTP
resource "openstack_lb_listener_v2" "main_haproxy_http_listener" {
  loadbalancer_id = openstack_lb_loadbalancer_v2.main_lb.id
  protocol        = "TCP"
  protocol_port   = 80
  description     = "HTTP listener of the main HAPROXY instances"
  name            = "main_haproxy_http_listener"
  admin_state_up  = true
}

resource "openstack_lb_pool_v2" "main_haproxy_http_pool" {
  listener_id = openstack_lb_listener_v2.main_haproxy_http_listener.id
  protocol    = "PROXYV2"
  lb_method   = "LEAST_CONNECTIONS"
  name        = "main-haproxy-lb-http"
  description = "Pool for the HTTP listener of the main HAPROXY instances"
  persistence {
    type = "SOURCE_IP"
  }
  admin_state_up = true
}

resource "openstack_lb_members_v2" "main_haproxy_http_pool_members" {
  pool_id = openstack_lb_pool_v2.main_haproxy_http_pool.id
  member {
    name          = "haproxy l7 1"
    address       = var.basic_services_ip.haproxy_l7_1
    protocol_port = 80
  }
  member {
    name          = "haproxy l7 2"
    address       = var.basic_services_ip.haproxy_l7_2
    protocol_port = 80
  }
}

resource "openstack_lb_monitor_v2" "main_haproxy_http_monitor" {
  pool_id        = openstack_lb_pool_v2.main_haproxy_http_pool.id
  name           = "main_haproxy_http_monitor"
  type           = "HTTP"
  http_method    = "GET"
  url_path       = "/_haproxy_health_check"
  expected_codes = "200"
  delay          = 20
  timeout        = 5
  max_retries    = 3
  admin_state_up = true
}

# Main HAPROXY HTTPS
resource "openstack_lb_listener_v2" "main_haproxy_https_listener" {
  loadbalancer_id        = openstack_lb_loadbalancer_v2.main_lb.id
  protocol               = "TCP"
  protocol_port          = 443
  description            = "HTTPS listener of the main HAPROXY instances"
  name                   = "main_haproxy_https_listener"
  timeout_client_data    = 3600000
  timeout_member_connect = 10000
  timeout_member_data    = 7200000
  admin_state_up         = true
}

resource "openstack_lb_pool_v2" "main_haproxy_https_pool" {
  listener_id = openstack_lb_listener_v2.main_haproxy_https_listener.id
  protocol    = "PROXYV2"
  lb_method   = "LEAST_CONNECTIONS"
  name        = "main-haproxy-lb-https"
  description = "Pool for the HTTPS listener of the main HAPROXY instances"
  persistence {
    type = "SOURCE_IP"
  }
  admin_state_up = true
}

resource "openstack_lb_members_v2" "main_haproxy_https_pool_members" {
  pool_id = openstack_lb_pool_v2.main_haproxy_https_pool.id
  member {
    name          = "haproxy l7 1"
    address       = var.basic_services_ip.haproxy_l7_1
    protocol_port = 443
  }
  member {
    name          = "haproxy l7 2"
    address       = var.basic_services_ip.haproxy_l7_2
    protocol_port = 443
  }
}

resource "openstack_lb_monitor_v2" "main_haproxy_https_monitor" {
  pool_id        = openstack_lb_pool_v2.main_haproxy_https_pool.id
  name           = "main_haproxy_https_monitor"
  type           = "HTTPS"
  http_method    = "GET"
  url_path       = "/_haproxy_health_check"
  expected_codes = "200"
  delay          = 20
  timeout        = 5
  max_retries    = 3
  admin_state_up = true
}

output "main_loadbalancer_ip" {
  description = "Main Load balancer IP address"
  value       = openstack_lb_loadbalancer_v2.main_lb.vip_address
}

