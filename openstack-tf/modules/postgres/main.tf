####################################################################################################################################################################
# 
#   Optional configurations for networking on Postgres 
#
####################################################################################################################################################################

# Security group for ingress to PostgreSQL
resource "openstack_networking_secgroup_v2" "postgresql_networking_ingress" {
  count = length(var.postgres_networking_data["networking_security_group_name"]) > 0 ? 1 : 0

  name                 = "${var.postgres_networking_data["networking_security_group_name"]}_ingress"
  delete_default_rules = true
  description          = "Access to the PostgreSQL service using the dedicated network"
}

# Ingress rule for PostgreSQL
resource "openstack_networking_secgroup_rule_v2" "postgresql_ingress_rule" {
  count = length(var.postgres_networking_data["networking_security_group_name"]) > 0 ? 1 : 0

  security_group_id = openstack_networking_secgroup_v2.postgresql_networking_ingress[0].id
  description       = "Allow connections to port ranges ${var.postgres_networking_data["port_range_min"]} / ${var.postgres_networking_data["port_range_max"]} from the dedicated ${var.postgres_networking_data["network_cidr"]} network"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = var.postgres_networking_data["port_range_min"]
  port_range_max    = var.postgres_networking_data["port_range_max"]
  remote_ip_prefix  = var.postgres_networking_data["network_cidr"]
}

# Security group for egress from VM to PostgreSQL server
resource "openstack_networking_secgroup_v2" "postgresql_egress" {
  count = length(var.postgres_networking_data["networking_security_group_name"]) > 0 ? 1 : 0

  name                 = "${var.postgres_networking_data["networking_security_group_name"]}_egress"
  delete_default_rules = true
  description          = "Access to the shared PostgreSQL service from the VM port in the dedicated network"
}

# Egress rule for PostgreSQL (TCP)
resource "openstack_networking_secgroup_rule_v2" "postgresql_egress_rule_tcp" {
  count = length(var.postgres_networking_data["networking_security_group_name"]) > 0 ? 1 : 0

  security_group_id = openstack_networking_secgroup_v2.postgresql_egress[0].id
  description       = "Allow egress connections to port/s on the PostgreSQL server"
  direction         = "egress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = var.postgres_networking_data["port_range_min"]
  port_range_max    = var.postgres_networking_data["port_range_max"]
  remote_ip_prefix  = var.postgres_networking_data["server_cidr"]
}

# Egress rule for PostgreSQL (ICMP)
resource "openstack_networking_secgroup_rule_v2" "postgresql_egress_rule_icmp" {
  for_each = length(var.postgres_networking_data["networking_security_group_name"]) > 0 ? { "egress_icmp_rule" : var.postgres_networking_data } : {}

  security_group_id = openstack_networking_secgroup_v2.postgresql_egress[0].id
  description       = "Allow ICMP traffic to the PostgreSQL server"
  direction         = "egress"
  ethertype         = "IPv4"
  protocol          = "icmp"
  remote_ip_prefix  = var.postgres_networking_data["server_cidr"]
}

####################################################################################################################################################################
# 
#   Postgres instance configurations
#
####################################################################################################################################################################

#Instance volume
resource "openstack_blockstorage_volume_v3" "instance_data_volume" {
  for_each = var.postgres_instance_data
  name     = each.value.volume.name
  size     = each.value.volume.size
}

# Generic postgres_service instance
resource "openstack_compute_instance_v2" "postgres_service" {
  for_each                = var.postgres_instance_data
  name                    = each.value.name
  availability_zone_hints = module.common_variables.availability_zone_no_gpu_name
  flavor_name             = each.value.flavor
  key_pair                = module.ssh_settings.ssh_key_name
  security_groups         = each.value.security_groups
  block_device {
    uuid                  = each.value.image_ref.uuid
    source_type           = "image"
    volume_size           = each.value.image_volume_size
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = false
  }

  # Creates the networks according to input networks 
  dynamic "network" {
    for_each = each.value.networks
    content {
      name = network.value
    }
  }

  # Creates the networks according to input networks 
  dynamic "network" {
    for_each = length(var.postgres_networking_data["server_ip"]) > 0 ? { "server_ip" : var.postgres_networking_data["server_ip"] } : {}
    content {
      name        = var.postgres_networking_data["network_name"]
      fixed_ip_v4 = var.postgres_networking_data["server_ip"]
    }
  }

  # Creates the scheduler_hints (i.e. server groups) according to input server_groups_ids
  dynamic "scheduler_hints" {
    for_each = each.value.server_groups_ids
    content {
      group = scheduler_hints.value
    }
  }

  # user_data script used
  user_data = file("${each.value.image_ref.user_data_file}")
  # Do not replace the instance when the ssh key changes
  lifecycle {
    ignore_changes = [
      # Ignore changes to tags, e.g. because a management agent
      # updates these based on some ruleset managed elsewhere.
      key_pair, user_data, network
    ]
  }
}

# Attach the additional volume
resource "openstack_compute_volume_attach_v2" "attach_volume" {
  for_each    = var.postgres_instance_data
  instance_id = openstack_compute_instance_v2.postgres_service[each.key].id
  volume_id   = openstack_blockstorage_volume_v3.instance_data_volume[each.key].id
  device      = each.value.volume.device
  depends_on  = [openstack_compute_instance_v2.postgres_service]
}
