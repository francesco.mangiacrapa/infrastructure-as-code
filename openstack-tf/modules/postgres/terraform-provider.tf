# Define required providers
terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.54.0"
    }
  }
}

# SSH settings
module "ssh_settings" {
  source = "../../modules/ssh-key-ref"
}

# Global variables (constants, really)
module "common_variables" {
  source = "../../modules/common_variables"
}

