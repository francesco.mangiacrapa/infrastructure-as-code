#
# Liferay nodes
#
#
# Security group
#
resource "openstack_networking_secgroup_v2" "liferay_cluster_traffic" {
  name                 = "liferay_cluster_traffic"
  delete_default_rules = "true"
  description          = "Traffic between the Liferay cluster nodes"
}

resource "openstack_networking_secgroup_rule_v2" "traffic_between_liferay_nodes" {
  count             = var.liferay_data.vm_count
  security_group_id = openstack_networking_secgroup_v2.liferay_cluster_traffic.id
  description       = "TCP traffic between liferay nodes"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  remote_ip_prefix  = join("/", [element(var.liferay_ip_addrs.*, count.index), "32"])
}

resource "openstack_networking_secgroup_rule_v2" "udp_traffic_between_liferay_nodes" {
  count             = var.liferay_data.vm_count
  security_group_id = openstack_networking_secgroup_v2.liferay_cluster_traffic.id
  description       = "UDP traffic between liferay nodes"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "udp"
  remote_ip_prefix  = join("/", [element(var.liferay_ip_addrs.*, count.index), "32"])
}

resource "openstack_networking_secgroup_rule_v2" "egress_traffic_between_liferay_nodes" {
  count             = var.liferay_data.vm_count
  security_group_id = openstack_networking_secgroup_v2.liferay_cluster_traffic.id
  description       = "egress TCP traffic between liferay nodes"
  direction         = "egress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  remote_ip_prefix  = join("/", [element(var.liferay_ip_addrs.*, count.index), "32"])
}

resource "openstack_networking_secgroup_rule_v2" "egress_udp_traffic_between_liferay_nodes" {
  count             = var.liferay_data.vm_count
  security_group_id = openstack_networking_secgroup_v2.liferay_cluster_traffic.id
  description       = "egress UDP traffic between liferay nodes"
  direction         = "egress"
  ethertype         = "IPv4"
  protocol          = "udp"
  remote_ip_prefix  = join("/", [element(var.liferay_ip_addrs.*, count.index), "32"])
}

resource "openstack_networking_secgroup_rule_v2" "igmp_ingress_between_liferay_nodes" {
  security_group_id = openstack_networking_secgroup_v2.liferay_cluster_traffic.id
  description       = "Ingress IGMP traffic between liferay nodes"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "igmp"
  remote_ip_prefix  = "0.0.0.0/0"
}

resource "openstack_networking_secgroup_rule_v2" "igmp_egress_between_liferay_nodes" {
  security_group_id = openstack_networking_secgroup_v2.liferay_cluster_traffic.id
  description       = "Egress IGMP traffic between liferay nodes"
  direction         = "egress"
  ethertype         = "IPv4"
  protocol          = "igmp"
  remote_ip_prefix  = "0.0.0.0/0"
}

#
# Object storage container
#
# Creating object bucket to store avatars
#
# Note: No S3 for the time being
# resource "openstack_objectstorage_container_v1" "liferay" {
#   name   = "liferay-data"
#   versioning = true
# }

#
# Server group
#
resource "openstack_compute_servergroup_v2" "liferay" {
  name     = "liferay"
  policies = [var.liferay_data.affinity_policy]
}

# Instance(s)
resource "openstack_compute_instance_v2" "liferay" {
  count                   = var.liferay_data.vm_count
  name                    = format("%s-%02d", var.liferay_data.srv_name, count.index + 1)
  availability_zone_hints = module.common_variables.availability_zones_names.availability_zone_no_gpu
  flavor_name             = var.liferay_data.vm_flavor
  key_pair                = module.ssh_settings.ssh_key_name
  security_groups         = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, openstack_networking_secgroup_v2.liferay_cluster_traffic.name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers,module.common_variables.security_group_list.nfs_share_no_ingress]
  scheduler_hints {
    group = openstack_compute_servergroup_v2.liferay.id
  }
  block_device {
    uuid                  = data.terraform_remote_state.privnet_dns_router.outputs.ubuntu_1804.uuid
    source_type           = "image"
    volume_size           = var.liferay_data.boot_vol_size
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = false
  }

  network {
    name        = data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name
    fixed_ip_v4 = var.liferay_ip_addrs.* [count.index]
  }
  network {
    name = module.common_variables.shared_postgresql_server_data.network_name
  }
  
  user_data = file("${data.terraform_remote_state.privnet_dns_router.outputs.ubuntu1804_data_file}")
  # Do not replace the instance when the ssh key changes
  lifecycle {
    ignore_changes = [
      # Ignore changes to tags, e.g. because a management agent
      # updates these based on some ruleset managed elsewhere.
      key_pair, user_data, network
    ]
  }
}

resource "openstack_dns_recordset_v2" "cdn_dns_recordset" {
  for_each = var.liferay_recordsets
  zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
  name        = each.value.name
  description = each.value.description
  ttl         = 8600
  type        = "CNAME"
  records     = [local.cname_target]
}

locals {
  cname_target     = "main-lb.${data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name}"
}

#
# Ports in the timescaleDB network
resource "openstack_networking_port_v2" "liferay_timescaledb_port" {
  count          = var.liferay_data.vm_count
  name           = format("%s-%02d", var.liferay_data.srv_name, count.index + 1)
  network_id     = data.terraform_remote_state.timescaledb.outputs.timescaledb_net.id
  admin_state_up = "true"
  fixed_ip {
    subnet_id = data.terraform_remote_state.timescaledb.outputs.timescaledb_subnet.id
  }
}

resource "openstack_networking_port_secgroup_associate_v2" "liferay_timescaledb_port_secgroup" {
  count              = var.liferay_data.vm_count
  port_id            = openstack_networking_port_v2.liferay_timescaledb_port[count.index].id
  security_group_ids = [data.terraform_remote_state.privnet_dns_router.outputs.nfs_share_no_ingress_secgroup_id]
}


resource "openstack_compute_interface_attach_v2" "timescaledb_port_to_liferay" {
  count       = var.liferay_data.vm_count
  instance_id = openstack_compute_instance_v2.liferay[count.index].id
  port_id     = openstack_networking_port_v2.liferay_timescaledb_port[count.index].id
}

#
# Manila NFS Share
#
# Managers
resource "openstack_networking_port_v2" "liferay_nfs_port" {
  count          = var.liferay_data.vm_count
  name           = format("%s-%02d", var.liferay_data.srv_name, count.index + 1)
  network_id     = data.terraform_remote_state.privnet_dns_router.outputs.storage_nfs_network_id
  admin_state_up = "true"
  fixed_ip {
    subnet_id = data.terraform_remote_state.privnet_dns_router.outputs.storage_nfs_subnet_id
  }
}

resource "openstack_networking_port_secgroup_associate_v2" "liferay_nfs_port_secgroup" {
  count              = var.liferay_data.vm_count
  port_id            = openstack_networking_port_v2.liferay_nfs_port[count.index].id
  security_group_ids = [data.terraform_remote_state.privnet_dns_router.outputs.nfs_share_no_ingress_secgroup_id]
}

resource "openstack_compute_interface_attach_v2" "nfs_port_to_liferay" {
  count       = var.liferay_data.vm_count
  instance_id = openstack_compute_instance_v2.liferay[count.index].id
  port_id     = openstack_networking_port_v2.liferay_nfs_port[count.index].id
}

# Create a NFS share
resource "openstack_sharedfilesystem_share_v2" "liferay_static" {
  name        = var.liferay_data.share_name
  description = var.liferay_data.share_description
  share_proto = "NFS"
  size        = 30
}

# Allow access to the NFS share
resource "openstack_sharedfilesystem_share_access_v2" "liferay_nfs_share_access" {
  count       = var.liferay_data.vm_count
  share_id     = openstack_sharedfilesystem_share_v2.liferay_static.id
  access_type  = "ip"
  access_to    = openstack_compute_interface_attach_v2.nfs_port_to_liferay[count.index].fixed_ip
  access_level = "rw"
}
