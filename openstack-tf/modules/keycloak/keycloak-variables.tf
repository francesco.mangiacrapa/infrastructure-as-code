variable "keycloak_data" {
  type = map(string)
  default = {
    affinity_policy = "soft-anti-affinity"
    srv_name        = "keycloak"
    vm_count        = 1
    vm_flavor       = "m1.medium"
    boot_vol_size   = 10
    share_description = "NFS share for the keycloak static data"
    share_name = "keycloak_nfs_share"  }
}

variable "keycloak_recordsets" {
  type = map(object({
    name        = string
    description = string
  }))
  default = {
    keycloak_dns_record = {
      name        = "",
      description = ""
    }
  }
}

variable "keycloak_object_store" {
  default = ""
}
