#
# keycloak nodes
#
#
# Security group
#
resource "openstack_networking_secgroup_v2" "keycloak_cluster_traffic" {
  name                 = "keycloak_cluster_traffic"
  delete_default_rules = "true"
  description          = "Traffic between the keycloak cluster nodes"
}

resource "openstack_networking_secgroup_rule_v2" "tcp_traffic_between_keycloak_nodes" {
  count             = var.keycloak_data.vm_count
  security_group_id = openstack_networking_secgroup_v2.keycloak_cluster_traffic.id
  description       = "TCP traffic between keycloak nodes"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  remote_ip_prefix  = join("/", [openstack_compute_instance_v2.keycloak[count.index].access_ip_v4, "32"])
}

resource "openstack_networking_secgroup_rule_v2" "udp_traffic_between_keycloak_nodes" {
  count             = var.keycloak_data.vm_count
  security_group_id = openstack_networking_secgroup_v2.keycloak_cluster_traffic.id
  description       = "UDP traffic between keycloak nodes"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "udp"
  remote_ip_prefix  = join("/", [openstack_compute_instance_v2.keycloak[count.index].access_ip_v4, "32"])
}

resource "openstack_networking_secgroup_rule_v2" "igmp_ingress_between_keycloak_nodes" {
  security_group_id = openstack_networking_secgroup_v2.keycloak_cluster_traffic.id
  description       = "Ingress IGMP traffic between keycloak nodes"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "igmp"
  remote_ip_prefix  = "0.0.0.0/0"
}

resource "openstack_networking_secgroup_rule_v2" "igmp_egress_between_keycloak_nodes" {
  security_group_id = openstack_networking_secgroup_v2.keycloak_cluster_traffic.id
  description       = "Egress IGMP traffic between keycloak nodes"
  direction         = "egress"
  ethertype         = "IPv4"
  protocol          = "igmp"
  remote_ip_prefix  = "0.0.0.0/0"
}

# Traffic from the main HAPROXY load balancers
# 
resource "openstack_networking_secgroup_v2" "traffic_from_haproxy_to_keycloak" {
  name                 = "traffic_to_keycloak_from_the_main_load_balancers"
  delete_default_rules = "true"
  description          = "Allow traffic from the main L7 HAPROXY load balancers to keycloak"
}

resource "openstack_networking_secgroup_rule_v2" "haproxy-l7-8443" {
  for_each          = { for ha_ip in data.terraform_remote_state.privnet_dns_router.outputs.main_haproxy_l7_ip : join("", [ha_ip]) => ha_ip }
  security_group_id = openstack_networking_secgroup_v2.traffic_from_haproxy_to_keycloak.id
  description       = "HTTPS traffic from HAPROXY L7 to Keycloak"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 9443
  port_range_max    = 9443
  remote_ip_prefix  = join("/", [each.value, "32"])
}

# Creating object bucket to store avatars
resource "openstack_objectstorage_container_v1" "keycloak_1" {
  name = var.keycloak_object_store
}

#
# Server group
#
resource "openstack_compute_servergroup_v2" "keycloak_server_group" {
  name     = "keycloak"
  policies = [var.keycloak_data.affinity_policy]
}

# Instance(s)
resource "openstack_compute_instance_v2" "keycloak" {
  count                   = var.keycloak_data.vm_count
  name                    = format("%s-%02d", var.keycloak_data.srv_name, count.index + 1)
  availability_zone_hints = module.common_variables.availability_zones_names.availability_zone_no_gpu
  flavor_name             = var.keycloak_data.vm_flavor
  key_pair                = module.ssh_settings.ssh_key_name
  security_groups         = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, openstack_networking_secgroup_v2.keycloak_cluster_traffic.name, openstack_networking_secgroup_v2.traffic_from_haproxy_to_keycloak.name]
  scheduler_hints {
    group = openstack_compute_servergroup_v2.keycloak_server_group.id
  }
  block_device {
    uuid                  = data.terraform_remote_state.privnet_dns_router.outputs.ubuntu_2204.uuid
    source_type           = "image"
    volume_size           = var.keycloak_data.boot_vol_size
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = false
  }

  network {
    name = data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name
  }
  network {
    name = module.common_variables.shared_postgresql_server_data.network_name
  }

  user_data = file("${data.terraform_remote_state.privnet_dns_router.outputs.ubuntu1804_data_file}")
  # Do not replace the instance when the ssh key changes
  lifecycle {
    ignore_changes = [
      # Ignore changes to tags, e.g. because a management agent
      # updates these based on some ruleset managed elsewhere.
      key_pair, user_data, network
    ]
  }
}

locals {
  cname_target = "main-lb.${data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name}"
}

resource "openstack_dns_recordset_v2" "keycloak_dns_recordset" {
  for_each    = var.keycloak_recordsets
  zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
  name        = each.value.name
  description = each.value.description
  ttl         = 8600
  type        = "CNAME"
  records     = [local.cname_target]
}

#
# Manila NFS Share
#
# Managers
resource "openstack_networking_port_v2" "keycloak_nfs_port" {
  count          = var.keycloak_data.vm_count
  name           = format("%s-%02d", var.keycloak_data.srv_name, count.index + 1)
  network_id     = data.terraform_remote_state.privnet_dns_router.outputs.storage_nfs_network_id
  admin_state_up = "true"
  fixed_ip {
    subnet_id = data.terraform_remote_state.privnet_dns_router.outputs.storage_nfs_subnet_id
  }
}

resource "openstack_networking_port_secgroup_associate_v2" "keycloak_nfs_port_secgroup" {
  count              = var.keycloak_data.vm_count
  port_id            = openstack_networking_port_v2.keycloak_nfs_port[count.index].id
  security_group_ids = [data.terraform_remote_state.privnet_dns_router.outputs.nfs_share_no_ingress_secgroup_id]
}

resource "openstack_compute_interface_attach_v2" "nfs_port_to_keycloak" {
  count       = var.keycloak_data.vm_count
  instance_id = openstack_compute_instance_v2.keycloak[count.index].id
  port_id     = openstack_networking_port_v2.keycloak_nfs_port[count.index].id
}

# Create a NFS share
resource "openstack_sharedfilesystem_share_v2" "keycloak_static" {
  name        = var.keycloak_data.share_name
  description = var.keycloak_data.share_description
  share_proto = "NFS"
  size        = 30
}

# Allow access to the NFS share
resource "openstack_sharedfilesystem_share_access_v2" "keycloak_nfs_share_access" {
  count       = var.keycloak_data.vm_count
  share_id     = openstack_sharedfilesystem_share_v2.keycloak_static.id
  access_type  = "ip"
  access_to    = openstack_compute_interface_attach_v2.nfs_port_to_keycloak[count.index].fixed_ip
  access_level = "rw"
}