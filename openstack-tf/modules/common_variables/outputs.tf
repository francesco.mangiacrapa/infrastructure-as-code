
output "main_region" {
  value = var.main_region
}

output "external_network" {
  value = var.external_network
}

output "external_network_id" {
  value = var.external_network.id
}

output "floating_ip_pools" {
  value = var.floating_ip_pools

}

output "resolvers_ip" {
  value = var.resolvers_ip
}

output "mtu_size" {
  value = var.mtu_size
}

output "availability_zones_names" {
  value = var.availability_zones_names
}

output "availability_zone_no_gpu_name" {
  value = var.availability_zones_names.availability_zone_no_gpu
}

output "availability_zone_with_gpu_name" {
  value = var.availability_zones_names.availability_zone_with_gpu
}

output "ssh_sources" {
  value = var.ssh_sources
}

output "networks_with_d4s_services" {
  value = var.networks_with_d4s_services
}

output "ubuntu_1804" {
  value = var.ubuntu_1804
}

output "ubuntu_2204" {
  value = var.ubuntu_2204
}

output "ubuntu_2404" {
  value = var.ubuntu_2404
}

output "centos_7" {
  value = var.centos_7
}

output "almalinux_9" {
  value = var.almalinux_9
}

output "ubuntu1804_data_file" {
  value = var.ubuntu1804_data_file
}

output "ubuntu2204_data_file" {
  value = var.ubuntu2204_data_file
}

output "ubuntu2404_data_file" {
  value = var.ubuntu2404_data_file
}

output "el7_data_file" {
  value = var.el7_data_file
}

output "ssh_jump_proxy" {
  value = var.ssh_jump_proxy
}

output "internal_ca_data" {
  value = var.internal_ca_data
}

output "prometheus_server_data" {
  value = var.prometheus_server_data
}

output "shared_postgresql_server_data" {
  value = var.shared_postgresql_server_data
}

output "haproxy_l7_data" {
  value = var.haproxy_l7_data
}

output "resource_registry_addresses" {
  value = var.resource_registry_addresses
}

output "smartexecutor_addresses" {
  value = var.smartexecutor_addresses
}

#Added by Francesco
output "policy_list" {
  value = var.policy_list
}

#Added by Francesco
output "flavor_list" {
  value = var.flavor_list
}

#Added by Francesco
output "security_group_list" {
  value = var.security_group_list
}

#Added by Francesco
output "networks_list" {
  value = var.networks_list
}

