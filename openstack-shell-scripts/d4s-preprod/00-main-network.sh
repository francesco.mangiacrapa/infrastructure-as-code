#!/usr/bin/env bash

. ./variables.sh

openstack --os-cloud ${os_infra} zone create --email postmaster@isti.cnr.it ${os_dns_zone}.
openstack --os-cloud ${os_infra} network create --description "D4Science Preprod private network (use this as the main network)" --no-share --mtu 8942 --dns-domain ${os_dns_zone}. ${os_infra}-cloud-main
openstack --os-cloud ${os_infra} subnet create --network ${os_infra}-cloud-main --dhcp --dns-nameserver 146.48.29.97 --dns-nameserver 146.48.29.98 --dns-nameserver 146.48.29.99  --subnet-range ${os_private_network} --allocation-pool start=${os_private_network_prefix}.${os_private_network_start_octet}.101,end=${os_private_network_prefix}.${os_private_network_allocation_end_octet}.255 --gateway ${os_private_gw} --dns-publish-fixed-ip ${os_infra}-cloud-sub
openstack --os-cloud ${os_infra} router create --description "D4Science Preprod main router" --external-gateway external-network ${os_infra}-cloud-external-router
openstack --os-cloud ${os_infra} router add subnet ${os_infra}-cloud-external-router ${os_infra}-cloud-sub
