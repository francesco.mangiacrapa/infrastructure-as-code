#!/usr/bin/env bash

# Basic project setup
# - Network
# - Subnet
# - Router
# - DNS zone

# Variables that must be included by all the scripts
#
os_infra=d4s-pre
os_dns_zone=cloud-pre.d4science.org
os_private_network_prefix="10.1"
os_private_network_start_octet=32
os_private_network_allocation_end_octet=35
os_private_network="${os_private_network_prefix}.${os_private_network_start_octet}.0/22"
os_private_gw=10.1.32.1

